var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 mix
     .scripts([
        'vendor/bootstrap-datepicker.min.js',
        'admin/admin.js',
        'admin/*'],
        'public/js/admin/admin.js')

     .less([
        'app.less',
        'admin/admin.less'],
        'public/css/admin/admin.css')

     .styles([])

     .sass("site.scss")

     .scripts('site.js', 'public/js/site.js')
     // Version
     //.version(['js/admin/admin.js', 'css/admin/admin.css'])

 ;

});
