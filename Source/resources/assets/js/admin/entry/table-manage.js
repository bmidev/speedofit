$(function() {
    var user_id = $('#user_id').val();
    
    $('#entryListing').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/datatable/entryProcessing/" + user_id,
            method:'POST'
        },
        columns: [{
                data: 'id'
            },{
                data: 'user_id'
            },{
                data: 'name'
            },{
                data: 'age'
            },{
                data: 'email'
            },{
                data: 'gender',
                render: function(data, type, row){
                    var str = '';
                    if(data == 1){
                        str = 'Male';
                    } else {
                        str = 'Female';
                    }
                    
                    return str;
                }
            },{
                data: 'is_swimmer',
                render: function(data, type, row){
                    var str = '';
                    if(data == 1){
                        str = '<b>Swimmer</b>';
                    } else {
                        str = 'Non Swimmer';
                    }
                    
                    return str;
                }
            },{
                data: 'score',
                render: function(data, type, row){
                    if(data > 75){
                        return '<strong>' + data + '%</strong>';
                    } else {
                        return data + '%';
                    }
                }
            },{
                sortable: false,
                data: 'id',
                render: function(data, type, row){
                    var str =
                        '<form method="POST" action="/entry/' + data + '" id="entryDelete' + data + '">' +
                            '<input type="hidden" name="_method" value="DELETE" /> ' +
                            '<a href="/entry/' + data +'">' +
                                '<button type="button" class="btn btn-info btn-sm">' +
                                    '<i class="glyphicon glyphicon-info-sign"></i>' +
                                    'Details' +
                                '</button>' +
                            '</a>' +
                            '<button type="button" class="btn btn-danger btn-sm deleteEntry" id="' + data + '" data-toggle="modal" data-target="#deleteModal">' +
                                '<i class="glyphicon glyphicon-trash"></i>' +
                                'Delete&nbsp;' +
                            '</button>' +
                        '</form>';
                    
                    return str;
                }
            }
        ]
    });
});