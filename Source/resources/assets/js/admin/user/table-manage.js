$(function() {
    $('#userListing').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/datatable/userProcessing",
            method:'POST'
        },
        columns: [{
                data: 'id'
            },{
                data: 'name'
            },{
                data: 'email'
            },{
                data: 'is_winner',
                render: function(data, type, row){
                    var str = '';
                    if(data == 1){
                        str = '<b>Yes</b>';
                    } else {
                        str = 'No';
                    }
                    
                    return str;
                }
            },{
                sortable: false,
                data: 'id',
                render: function(data, type, row){
                    var str =
                        '<form method="POST" action="/user/' + data + '" id="userDelete' + data + '">' +
                            '<input type="hidden" name="_method" value="DELETE" /> ' +
                            '<a href="/user/' + data +'">' +
                                '<button type="button" class="btn btn-info btn-sm">' +
                                    '<i class="glyphicon glyphicon-info-sign"></i>' +
                                    'Details' +
                                '</button>' +
                            '</a>' +
                            '<button type="button" class="btn btn-danger btn-sm deleteUser" id="' + data + '" data-toggle="modal" data-target="#deleteModal">' +
                                '<i class="glyphicon glyphicon-trash"></i>' +
                                'Delete&nbsp;' +
                            '</button>' +
                            '<button type="button" class="btn btn-success btn-sm markWinner" id="' + data + '"data-toggle="modal" data-target="#winnerModal">' +
                                '<i class="glyphicon glyphicon-star"></i>' +
                                'Winner' +
                            '</button>' + 
                        '</form>';
                    
                    return str;
                }
            }
        ]
    });
});