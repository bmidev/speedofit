$(function() {
    $('#questionListing').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/datatable/questionProcessing",
            method:'POST'
        },
        columns: [{
                data: 'is_active',
                render: function(data, type, row){
                    var str = '';
                    if(data == 1){
                        str = '<span class="label label-success">Active</span>';
                    } else {
                        str = '<span class="label label-default">In Active</span>';
                    }
                    
                    return str;
                }
            }, {
                data: 'id'
            },{
                data: 'question'
            },{
                data: 'is_swimmer',
                render: function(data, type, row){
                    var str = '';
                    if(data == 1){
                        str = '<b>Swimmer</b>';
                    } else {
                        str = 'Non Swimmer';
                    }
                    
                    return str;
                }
            },{
                sortable: false,
                data: 'id',
                render: function(data, type, row){
                    var str =
                        '<form method="POST" action="/question/' + data + '" id="questionDelete' + data + '">' +
                            '<input type="hidden" name="_method" value="DELETE" /> ' +
                            '<a href="/question/' + data + '/edit">' +
                                '<button type="button" class="btn btn-warning btn-sm"> ' +
                                    '<i class="glyphicon glyphicon-pencil"></i>' +
                                    'Edit' +
                                '</button>' +
                            '</a>' +
                            '<a href="/question/' + data +'">' +
                                '<button type="button" class="btn btn-info btn-sm">' +
                                    '<i class="glyphicon glyphicon-info-sign"></i>' +
                                    'Details' +
                                '</button>' +
                            '</a>' +
                            '<button type="button" class="btn btn-danger btn-sm deleteQuestion" id="' + data + '" data-toggle="modal" data-target="#deleteModal">' +
                                '<i class="glyphicon glyphicon-trash"></i>' +
                                'Delete&nbsp;' +
                            '</button>' +
                        '</form>';
                    
                    return str;
                }
            }
        ]
    });
});