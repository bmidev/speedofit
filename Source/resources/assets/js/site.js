var APP = APP || {};

APP.pushGaEvent = function (category, action, label) {
    if (typeof (ga) === 'function') {
        ga('send', 'event', category, action, label);
    }
};

window.fbAsyncInit = function() {
    FB.init({
      appId      : '1771385236416666',
      xfbml      : true,
      version    : 'v2.5'
    });
};

//window.fbAsyncInit = function() { 
//    FB.init({
//      appId      : '1771385236416666', // 1698441653760355
//      xfbml      : true,
//      version    : 'v2.5'
//    });
//};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).ready(function () {
	// Mobile Navigation
	$('.mobile-toggle').click(function() {
		if ($('header').hasClass('open-nav')) {
			$('header').removeClass('open-nav');
		} else {
			$('header').addClass('open-nav');
		}
	});
	$('nav.menu li a').click(function() {
		if ($('header').hasClass('open-nav')) {
			$('header').removeClass('open-nav');
		}
	});	
	
	//Carousal
	/*$('.home-banner').slick({
		dots: true,
		autoplay: true,
		speed: 300,
		adaptiveHeight: true,
		verticle: true
    });*/	
	
	if($("#wizard").length){
		$("#wizard").steps({
			headerTag: "h2",
			bodyTag: "section",
			cssClass: "quiz-data",
			titleTemplate: '<span class="number"><label>#index#</label></span>',
			transitionEffect: "slideLeft",
			enablePagination: false,
			onInit: function (event, currentIndex) {
				var errorMsg = '<div class="errorMsg"></div>';				
				$(".content ").append(errorMsg);
			},
			//event to be fired when previous step click
            onStepChanging: function (event, currentIndex, newIndex) {
				var errorMsg = 'Please select any one option.';
				$(".errorMsg ").text("");
                if(currentIndex == 0) {
					$(".errorMsg ").text("");
					var rdoVal = $("input:radio[name='is_swimmer']:checked").val();
					if( rdoVal == "0"){
						$('.quiz-data .steps').addClass('nonswimmer');
						return true;
					} else if( rdoVal == "1"){
						$('.quiz-data .steps').addClass('swimmer');
						return true;
						
					} else{						
						$(".errorMsg ").text(errorMsg);
						return false;	
					}
                } else if(currentIndex == 1){
					var rdoVal = $("input:radio[name='option[0]']:checked").val();					
					if( rdoVal == undefined){
						$(".errorMsg ").text(errorMsg);
						return false;
					}
					else{					
						return true;
					}
				} else if(currentIndex == 2){
					var rdoVal = $("input:radio[name='option[1]']:checked").val();					
					if( rdoVal == undefined){
						$(".errorMsg ").text(errorMsg);
						return false;
					}
					else{					
						return true;
					}
				} else if(currentIndex == 3){
					var rdoVal = $("input:radio[name='option[2]']:checked").val();
					if( rdoVal == undefined){
						$(".errorMsg ").text(errorMsg);
						return false;
					}
					else{					
						return true;
					}
				} else if(currentIndex == 4){
					var rdoVal = $("input:radio[name='option[3]']:checked").val();
					if( rdoVal == undefined){
						$(".errorMsg ").text(errorMsg);
						return false;
					}
					else{					
						return true;
					}
				} else if(currentIndex == 5){
					var rdoVal = $("input:radio[name='option[4]']:checked").val();
					if( rdoVal == undefined){
						$(".errorMsg ").text(errorMsg);
						return false;
					}
					else{					
						return true;
					}
				} else if(currentIndex == 6){
					var rdoVal = $("input:radio[name='option[5]']:checked").val();					
					if( rdoVal == undefined){
						$(".errorMsg ").text(errorMsg);
						return false;
					}
					else{					
						return true;
					}
				} 				
            }
		});
		
		$(document).on('click', '.next-level', function(e) {
			e.preventDefault();
			$("#wizard").steps('next');
		});
		$("#quiz-reg").on('submit', function(){
			var errorMsg = 'Please provide your correct information.';
			$(".errorMsg ").text("");
            var is_swimmer = $("input[name='is_swimmer']:checked").val();
			var txtName = $("input#txtName").val();
			var txtAge = $("input#txtAge").val();	
			var rdoGender = $("input:radio[name='gender']:checked").val();	
			var txtEmail = $("input#txtEmail").val();
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
			
			if( txtName == "" || $.isNumeric(txtAge) == false || rdoGender == undefined || testEmail.test(txtEmail) == false) {
				$(".errorMsg ").text(errorMsg);
				return false;
			} else {
                var responseStatus = false;
                var userData = {
                    'name' : txtName,
                    'age' : txtAge,
                    'gender' : rdoGender,
                    'email' : txtEmail,
                    'is_swimmer': is_swimmer
                };
                
                $.ajax({
                    type: 'POST',
                    url: APP.baseUrl +'/validate-data',
                    async: false,
                    data: userData,
                    success: function(result){
                        if(result.status === true){
                            responseStatus = true;
                        } else {
                            responseStatus = false;
                            errorMsg = result.message;
                            $(".errorMsg ").text(errorMsg);
                            return false;
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Oops! Something went wrong. Please try again.');
                        return false;
                    }
                });
                
                if(responseStatus === false){
                    return false;
                } else {
                    return true;
                }
			}
		});
	}
	/*$('#bgVideo').vide({
        mp4: APP.baseUrl + '/video/ocean',
        webm: APP.baseUrl + '/video/ocean',
        ogv: APP.baseUrl + '/video/ocean',
        poster: APP.baseUrl + '/video/ocean'
	});*/
    
    $('#user_preference').on('click', function(){
        var is_swimmer = $("input[name='is_swimmer']:checked").val();
        $.ajax({
            url: APP.baseUrl + "/fetch-question",
            data: 'is_swimmer=' + is_swimmer,
            success: function(result){
                $(result).each(function(index, element) {
                    $('#question_' + index).text(element.question);
                    $(element.answers).each(function(ansIndex, ansElement) {
                        $('#option' + index + '_' + ansIndex).val(ansElement.id);
                        $('#option' + index + '' + ansIndex).text(ansElement.answer);
                    });
                });
        }});
    });
    
    $('#fb-share').click(function(e){
        e.preventDefault();
		
        var participantID = $('#participantID').val();
		var comboID = $('#comboID').val();
        var participantName = $('#participantName').html();
        var participantFirstName = participantName.split(" ")[0];
        var participantScore = $('#participantScore').html();
        FB.ui({
            method: 'feed',
            name: participantFirstName + ' Take the Fit To Speedo Fit Challenge!',
            link: APP.baseUrl,
            picture: APP.baseUrl + '/images/user/' + comboID + '.jpg',
            caption: 'Speedo Fit Challenge',
            description: '',
            message: ''
        });
		
		APP.pushGaEvent('share', 'share facebook', 'Facebook Share');
		APP.pushGaEvent('share', 'share facebook', participantID);
    });

    $('#tw-share').click(function(e){
        e.preventDefault();
        
        var percent = $(this).data('percent');
        var message = encodeURIComponent("Hey, my Speedo Fitness quotient is " + percent + "%! Why don’t you take the Fit to Speedo Fit Challenge too at");
        var user_id = $(this).data('id');
        
        APP.twShare(APP.baseUrl, message, 'Twitter Share', user_id);
    });
});

APP.twShare = function (link, message, tracking_key, user_id) {
    if (!link) {
        link = APP.baseUrl;
    }

    window.open('https://twitter.com/share?text=' + message + '&url=' + link, 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    APP.pushGaEvent('share', 'share twitter', tracking_key);
    APP.pushGaEvent('share', 'share twitter', user_id);
};
