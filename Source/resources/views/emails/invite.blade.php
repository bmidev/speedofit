<!DOCTYPE html>
<html>
	<head>
		 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <title>Festool</title>
	</head>
	<body style="margin:0; padding:0; min-width:100%;">
		<table bgcolor="#dad9cf" align="center" cellspacing="0" cellpadding="0" border="0" style=" width:600px;">
			<tr>
				<td bgcolor="#dad9cf">
					<img style="display:block; width:100%; height:auto; border:0;" src="{{ asset('images/Festool_email1.jpg') }}" alt="Festool_email1.jpg" usemap="#link1">
					<map name="link1">
						 <area shape="rect" coords="304,21,342,55" target="_blank" href="http://www.facebook.com/festoolGB" alt="Facebook">
						 <area shape="rect" coords="261,23,299,56" target="_blank" href="https://twitter.com/festool_gb" alt="Twitter">
				     </map>
				</td>
			</tr>
			<tr>
				<td>
					<table align="center" cellspacing="0" cellpadding="0" border="0" style="width:600px;">
						<tr>
							<td bgcolor="#dad9cf" style="height:15px; width:36px;"></td>
							<td bgcolor="#44b749" style="height:15px; width:521px;"></td>
							<td bgcolor="#dad9cf" style="height:15px; width:43px;"></td>
						</tr>
						<tr>
							<td bgcolor="#dad9cf" style="width:36px;"></td>
							<td bgcolor="#ffffff" style="text-align:center; width:481px; padding:20px; color:#1e2129; font-family:verdana,san-serif;">
								@if(isset($invite_data))
									<h1 style="font-size:27px; margin-top: 1%;">Dear {{ $invite_data['name'] }}</h1>
								@else
									<h1 style="font-size:27px; margin-top: 1%;">Dear {{ 'Sammy Jo' }}</h1>
								@endif

								<p style="font-size:14px;">Your friend, {{ isset($inviter) ? $inviter : 'Arighna Sarkar' }} has invited you to enter our Festool 12 Tools of Christmas Competition where you have 12 chances to win fantastic Festool tools and prizes on a daily basis from the 11th-22nd December 2015.</p>
								<p style="font-size:14px;">All you have to do is simply visit <a href="http://www.festool12tools.co.uk" target="_blank">www.festool12tools.co.uk</a> and follow the instructions!</p>
								<p style="font-size:14px;">Tell your friends by forwarding them this email or sharing on Facebook <a href="http://www.facebook.com/festoolGB" target="_blank">www.facebook.com/festoolGB</a> and Twitter <a target="_blank" href="http://twitter.com/festool_gb">@Festool_GB</a></p>
							</td>
							<td bgcolor="#dad9cf" style="width:43px;"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td bgcolor="#dad9cf">
					<img style="display:block; width:100%; height:auto; border:0;" src="{{ asset('images/Festool_email2.jpg') }}" alt="Festool_email2.jpg">
				</td>
			</tr>
			<tr>
				<td bgcolor="#44b749" style="text-align:center; color:#1e2129; font-family:verdana,san-serif;">
					<br>
					<strong>
						<a href="{{ asset('pdf/tnc.pdf') }}" target="_blank" style="text-decoration:none; color:#1e2129; font-size:0.72em;">OFFICIAL RULES</a> |
						<a href="http://www.festool.co.uk/Privacy/Pages/Privacy-Online.aspx" target="_blank" style="text-decoration:none; color:#1e2129; font-size:0.72em;">PRIVACY POLICY</a> |
						<a href="https://www.festool.co.uk/" target="_blank" style="text-decoration:none; color:#1e2129; font-size:0.72em;">FESTOOL.CO.UK</a> |
						{{--<span style="font-size:0.72em;">POWERED BY </span><a href="http://brandmovers.co.uk" target="_blank" style="text-decoration:none; color:#1e2129; font-size:0.72em;">BRANDMOVERS</a>--}}
						<a href="http://brandmovers.co.uk" target="_blank" style="text-decoration:none; color:#1e2129; font-size:0.72em;">POWERED BY BRANDMOVERS</a>
					</strong>
					<br>
					<br>
				</td>				
			</tr>
			<!--<tr>
				<td bgcolor="#dad9cf">
					<img style="display:block; width:100%; height:auto; border:0;" src="{{ asset('images/Festool_email3.jpg') }}" alt="Festool_email3.jpg" usemap="#link">
					<map name="link">
						 <area shape="rect" coords="30,0,145,48" target="_blank" href="#" alt="Official Rules">
						 <area shape="rect" coords="150,0,270,48" target="_blank" href="#" alt="Privacy policy">
						 <area shape="rect" coords="275,0,400,48" target="_blank" href="#" alt="Festools">
						 <area shape="rect" coords="405,0,600,48" target="_blank" href="#" alt="Brandmovers">
				     </map>
				</td>
			</tr>-->
		</table>
	</body>
</html>