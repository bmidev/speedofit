<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Speedo Fit | Admin | Brandmovers</title>
    <link rel="icon" type="image/png" href="{{BASE_URL}}favicon.ico" />
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css" rel="stylesheet">
    
    <!-- Datatable Stylesheet -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
    
    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <style type="text/css">
            img.emoji {  margin: 0px !important;  display: inline !important;  }
            a {
                text-decoration: none !important;
            }
    </style>
    <script>
        var BASE_URL = "{{ BASE_URL }}";
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//twemoji.maxcdn.com/twemoji.min.js"></script>
</head>
<body>
    @if (!Auth::guest())
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <span class="navbar-brand">
                            Speedo
                        </span>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ url('user') }}">User</a></li>
                        <li><a href="{{ url('entry') }}">Entry</a></li>
                        <li><a href="{{ url('question') }}">Question</a></li>
                        <li><a href="{{ url('metric') }}">Metric</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ BASE_URL }}/admin/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    @endif
    
    <div class="container">
        @if(Session::has('message'))
            <div class="alert alert-info fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! Session::get('message') !!}
            </div>
        @endif
    </div>
    
    <div class="container">
        @yield('content')
    </div>

    <!-- Stylesheet -->
    @yield('stylesheet')
    <!-- Stylesheet -->
     
    <!-- Scripts -->
    <script>
        var APP = APP || {};
        APP.adminUrl = "<?php echo BASE_URL . 'admin/'; ?>";
    </script>
    @if(\Request::segment(1) === 'dashboard')
        <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script>
    @else
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    @endif
    <script src="http://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/admin/admin.js') }}"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    @yield('scripts')
    <!-- Scripts -->
</body>
</html>
