<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '289528447924104');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=289528447924104&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<title>Speedo: Fit to Speedo Fit Challenge</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    {{--<link href="{{ asset('css/vendor/slick.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('css/vendor/jquery.steps.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/site.css') }}?v=3" rel="stylesheet" type="text/css"/>
    <!-- FACEBOOK METAS -->
    {{--<meta property="fb:app_id" content="{{ FB_APP_ID }}" />--}}
    <meta property="og:title" content="Speedo Fit Challenge"/>
    <meta property="og:description" content="Are you fit? Are you ready to be speedo fit? Take the Fit to Speedo Fit Challenge and stand a chance to win an amazing trip and Speedo merchandise!"/>
    <meta property="og:url" content="{{ url('/') . '/' }}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:image" content="{{ url('/').'/' }}images/share-image.jpg" />
    
    <script src="{{  asset('js/modernizr.custom.js') }}"></script>
</head>

<body>
<div class="main-container">
    <header>
        <div class="wrapper-area clear">            
            <div class="logo">
                <a href="{{BASE_URL}}"><img src="{{ asset('images/logo-speedo.png') }}" alt=""/></a>
            </div>
            {{--<div class="mobile-toggle"></div>--}}
            {{--<nav class="menu">--}}
                {{--<ul>--}}
                    {{--<li><a href="{{BASE_URL}}" @if(\Request::segment(1) === null) {!! 'class=active' !!} @else {!! 'class=in-active' !!} @endif>Home</a></li>--}}
                    {{--<li><a href="{{BASE_URL}}/about-us" @if(\Request::segment(1) === 'about-us') {!! 'class=active' !!} @else {!! 'class=in-active' !!} @endif>About Us</a></li>--}}
                    {{--<li><a href="{{BASE_URL}}/prizes" @if(\Request::segment(1) === 'prizes') {!! 'class=active' !!} @else {!! 'class=in-active' !!} @endif>Prizes</a></li>--}}
                    {{--<li><a href="{{BASE_URL}}/quiz" @if(\Request::segment(1) === 'quiz') {!! 'class=active' !!} @else {!! 'class=in-active' !!} @endif>Challenge</a></li>--}}
                {{--</ul>--}}
            {{--</nav>--}}
        </div>
    </header>

    @yield('content')    
</div>
    <footer class="clear">
        <div class="wrapper-area">
            <div class="copyright clear">
                <div class="left">
                    <a href="{{BASE_URL}}/about-us">About Us</a> | 
                    <a href="{{BASE_URL}}/terms-and-condition">Terms &amp; Conditions</a> | 
                    <a href="{{BASE_URL}}/privacy-policy">Privacy</a> |
                	<a href="https://www.facebook.com/SpeedoIndia/app/1535649840068804/" target="_blank">Store locator</a> 
                </div>
                <div class="right">Follow Us
                    <div class="social">
                        <ul>
                            <li><a href="https://twitter.com/SpeedoIndia" target="_blank" class="ig"></a></li>
                            <li><a href="https://www.facebook.com/SpeedoIndia/" target="_blank" class="fb"></a></li>
                            <li><a href="https://www.instagram.com/speedoindia/" target="_blank" class="tw"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="powered">
                Powered by <a href="http://www.brandmovers.in/" target="_blank">Brandmovers India</a>
            </div>
        </div>
    </footer>
<div class="hide">
    
</div>

<!--GA code -->
<!-- TODO Need GA for Speedo Fit -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75418692-1', 'auto');
  ga('send', 'pageview');
</script>
<!--GA code end-->
<div id="fb-root"></div>
<!-- Scripts -->
<script>
    var APP = APP || {};
    APP.baseUrl = "{{ BASE_URL }}";
</script>
<script src="{{ asset('js/jquery.js') }}"></script> 
{{--
	<script src="{{ asset('js/vendor/slick.min.js') }}"></script>
	<script src="{{ asset('js/vendor/jquery.vide.js') }}"></script>
--}}
<script src="{{ asset('js/vendor/jquery.cookie-1.3.1.js') }}"></script>
<script src="{{ asset('js/vendor/jquery.steps.js') }}"></script>

<script src="{{ asset('js/site.js') }}?v=2"></script> 
@yield('scripts')
</body>
</html>
