<section id="thankyou" class="form-container">
    <div class="social-links inner">
        <ul>
            <li><a class="fb" href="javascript:;"></a></li>
            <li><a class="tw"
                   href="https://twitter.com/intent/tweet?text={{ urlencode(utf8_encode(config('twitter.share_copy'))) }}&url={{ config('twitter.share_url') }}"></a>
            </li>
        </ul>
    </div>
    <h1>Thank you !</h1>
    <p>Your invitation has been sent.</p>
    <p>You've received a second into today's giveaway.</p>
    <p>Don't forget to visit the Festool UK <a style="color: #0000AA;" href="http://www.facebook.com/festoolGB" target="_blank">Facebook</a> and <a style="color: #0000AA;" target="_blank" href="http://twitter.com/festool_gb">Twitter</a> page for competition updates!</p>
    
    <div class="buttons">
    	<button class="close">Close</button>
    </div>
</section>