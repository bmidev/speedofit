<section id="login" class="form-container">
    @if($prize_today == null)
        <h1>There are no active prize.</h1>
    @else
        <div class="clear">
            <div class="left">
                <img src="{{ asset('images/prizes/original') . '/' . $prize_today->image }}" alt=""/>
            </div>
            <div class="right">
                @if($prize_today->star)
                    <h1>{{ date('jS', strtotime($prize_today->unlock_date)) }} of December - <strong>*STAR PRIZE*</strong> {{ $prize_today->name }}</h1>
                @else
                    <h1>{{ date('jS', strtotime($prize_today->unlock_date)) }} of December - {{ $prize_today->name }}</h1>
                @endif
                <p>{{ $prize_today->description }}</p>
                <p>Enter your email below for today's prize draw</p>
                <form action="" method="post" id="login-form" class="form clear">
                    <input name="email" type="text" required placeholder="Email address" autocomplete="off"/>
                    <input type="hidden" name="prize_id" value="{{ isset($prize_today) ? $prize_today->id  : ''}}"/>
                    <input type="submit" value="Enter" class="submit">
                    <div class="error" style="display: none;"></div>
                </form>
            </div>
        </div>
    @endif
</section>