<section id="thankyou-invite" class="form-container">    
    <div class="social-links inner">
        <ul>
            <li><a class="fb" href="javascript:;"></a></li>
            <li><a class="tw"
                   href="https://twitter.com/intent/tweet?text={{ urlencode(utf8_encode(config('twitter.share_copy'))) }}&url={{ config('twitter.share_url') }}"></a>
            </li>
        </ul>
    </div>
    <h1>Thank you!</h1>
    <p>You have been entered to win today's tool prize</p>
    <p>Gain an additional entry by inviting a friend to participate via email. <br>
        You are allowed one per day.</p>
    <form action="" method="post" id="invite-form" class="">
        <input name="name" type="text" required placeholder="friend's first and last name" />
        <input name="email" type="text" required placeholder="friend's email address" />
        <div class="error" style="display: none;"></div>
        <div class="buttons">
            <input type="submit" value="Invite" class="submit">
        </div>
        <div class="skip"><a href="javascript:;" class="no-thankyou">no thank you</a></div>
    </form>
    
</section>