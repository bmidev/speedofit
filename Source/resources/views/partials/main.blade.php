@extends('layouts.master')
@section('content')
    <section class="sub-header">
        <div class="page-container">
            <h2>12 Tools of Christmas</h2>
            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse quis odio sed erat placerat scelerisque. Curabitur id ligula eu lorem tempus dapibus.</p>--}}
            
                @if(time() > strtotime(CONTEST_END) && time() <= strtotime(END_DATE))
                    <p>This competition is now closed. Congratulations to all of our winners! A big thank you to everyone who entered.
                    <br><br>
                    We would like to wish all of our customers a very Merry Christmas and a Happy New Year!
                    <br><br>
                    From Festool UK</p>
                @else
                   <p>We would like to wish all of our customers a very <strong>Merry Festool Christmas!</strong></p>
            <p>We are spreading a little Festool magic by offering you the chance to win a different tool each day. Simply click on the Festool advent calendar to see the tool prize of the day, enter your details to be in with a chance of winning!</p>
            <p>Good luck and Merry Christmas from Festool UK</p>
            <div class="link"><a href="#entry-tools">Every entry is a chance to win.</a></div>
                @endif
        </div>
    </section>
<section class="contest-area">
	<div class="page-container">
    	<div class="fog"></div>
        <div class="holly-ivy1"><img src="{{ asset('images/img-holly-ivy1.png') }}" alt=""/></div>
        <div class="holly-ivy2"><img src="{{ asset('images/img-holly-ivy2.png') }}" alt=""/></div>
		<div class="grand-prize" id="grand-prize-section">
        	<div class="strip"></div>
			<figure class="clear">
            	<img src="{{ asset('images/img-grand-prize.jpg') }}" alt=""/>
            	<figcaption>
                	<h1>Grand Prize</h1>
                    {{--<h2>Festool Kapex FS120</h2>--}}
                    {{--<p>Lorem ipsum dolor sit amet, ad est aperiam singulis. Qui no solum iuvaret. Oratio nusquam ex ius, laoreet philosophia vis ad, harum.</p>--}}
                    <p>Congratulations to our lucky winner PAUL C on winning &pound;750 of tools!</p>

                </figcaption>
            </figure>
        </div>
        <div class="tools clear" id="entry-tools">
            {{--*/ $i = 1 /*--}}
            @foreach($all_prizes as $row)
                @if($row->status == 'present')
                    <article>
                        <div class="product active" style="display:block;">
                            <img src="{{ asset('images/prizes/original') . '/' . $row->image }}" alt=""/>
                            <span class="tools-name">{{ $row->short_name }}</span>
                            <span class="date"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>
                            <!--Date active-->
                            <div class="active-area popup">
                                <span class="enter">Enter now</span>
                            </div>
                        </div>
                    </article>

                @elseif($row->status == 'past')
                    <article>
                        <div class="product disabled" style="display:block;">
                            <img src="{{ asset('images/prizes/original') . '/' . $row->image }}" alt=""/>
                            <span class="tools-name">{{ $row->short_name }}</span>
                            @if($i % 3 == 0)
                                <span class="date hexagon"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>
                            @else
                                <span class="date"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>
                            @endif
                            {{--<span class="date"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>--}}
                            <!--Date passed-->
                                @if(isset($row->winner_first_name))
                                    <span class="winner">{{ $row->winner_first_name . ' ' . str_limit($row->winner_last_name, 1, '') }}</span>
                                {{--@else--}}
                                    {{--<span class="winner">Sammy {{ str_limit('Jo Mascarenhas', 1, '') }}</span>--}}
                                @endif
                            <div class="overlay">
                                <span class="done"></span>
                            </div>
                        </div>
                    </article>
                @else
                    <article>
                        <div class="front">
                            <img src="{{ asset('images/prizes/front') . '/' . $row->front_image }}" alt=""/>
                            @if($i % 3 == 0)
                                <span class="date hexagon"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>
                            @else
                                <span class="date"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>
                            @endif
                            <span class="month">December</span>
                        </div>
                        <div class="product">
                            <img src="{{ asset('images/prizes/original') . '/' . $row->image }}" alt=""/>
                            <span class="tools-name">{{ $row->short_name }}</span>
                            <span class="date"><label>{{ date('d', strtotime($row->unlock_date)) }}</label></span>
                        </div>
                    </article>

                @endif
                    {{--*/ $i++ /*--}}
            @endforeach
        	{{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/11dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">11</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/11dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">90 yrs watch</span>--}}
                    {{--<span class="date">11</span>--}}
                    {{--<!--Date passed-->--}}
                    {{--<span class="winner">Sammy Jo Mascarenhas</span>--}}
                    {{--<div class="overlay">--}}
                    	{{--<span class="done"></span>--}}
                    {{--</div>--}}
                    {{--<!--Date active-->--}}
                    {{--<div class="active-area">--}}
                   		{{--<span class="enter">Enter now</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/12dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">12</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/12dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">BHC 18</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front"><img src="{{ asset('images/prizes/front/13dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">13</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/13dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">SYS DUO</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/14dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">14</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/14dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">CTL Midi</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/15dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">15</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/15dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">TS 55</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front"><img src="{{ asset('images/prizes/front/16dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">16</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/16dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">PDC 18/4</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/17dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">17</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/17dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">OS 400</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/18dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">18</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/18dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">ETS EC 125/3</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front"><img src="{{ asset('images/prizes/front/19dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">19</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/19dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">PSC 420</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/20dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">20</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/20dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">HKC 55</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front">--}}
                	{{--<img src="{{ asset('images/prizes/front/21dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">21</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/21dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">DF 500</span>--}}
                {{--</div>--}}
            {{--</article>--}}
            {{--<article>--}}
            	{{--<div class="front"><img src="{{ asset('images/prizes/front/22dec.jpg') }}" alt=""/>--}}
                	{{--<span class="date">22</span>--}}
                    {{--<span class="month">December</span>--}}
                {{--</div>--}}
                {{--<div class="product">--}}
                	{{--<img src="{{ asset('images/prizes/original/22dec.jpg') }}" alt=""/>--}}
                    {{--<span class="tools-name">KS 120</span>--}}
                {{--</div>--}}
            {{--</article>--}}
        </div>
    </div>
</section>

@endsection

@section('scripts')
@endsection