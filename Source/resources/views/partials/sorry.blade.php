<section id="sorry" class="form-container">
    <h1>Sorry !</h1>
    <p>You can only enter once per day per email address.</p>
    <div class="buttons">
        <button class="close">Close</button>
    </div>
</section>