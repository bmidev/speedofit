<section id="register" class="form-container">
    <h1>Registration - Step 2</h1>
    <p>Please enter your details below to be in with a chance of winning today's tool! </p>
    <form action="" method="post" id="register-form" class="">
        <input name="first_name" type="text" required class="first-name" placeholder="first name"/>
        <input name="last_name" type="text" required class="last-name" placeholder="last name"/>
        <input name="phone" id="txtPhone" type="text" required class="clear" placeholder="phone number (e.g. 0161 123 4567)" maxlength="16"/>
        <p>We will only use your phone number to contact you if you are a winner</p>
        <div class="approval clear">
            <input name="over_18" type="checkbox" id="over_18" class="css-checkbox">
            <label for="over_18" class="css-label">I am over 18 years old</label>

            <input name="tnc" type="checkbox" id="tnc" class="css-checkbox">
            <label for="tnc" class="css-label">I agree to the <a href="{{ BASE_URL }}tnc" target="_blank">terms and conditions</a></label>

            <input name="optin" type="checkbox" id="optin" class="css-checkbox" checked>
            <label for="optin" class="css-label">Yes, please reach out to me with marketing materials</label>
        </div>
        <div class="error" style="display: none;"></div>
        <div class="buttons">
            <input type="submit" value="Enter" class="submit">
        </div>
    </form>
</section>