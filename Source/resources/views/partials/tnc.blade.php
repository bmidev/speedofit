@extends('layouts.master')
@section('content')
<section class="contest-area rules-data">
        <div class="page-container">
        	<div class="fog"></div>
            <div class="holly-ivy1"><img src="{{ asset('images/img-holly-ivy1.png') }}" alt=""/></div>
        	<div class="holly-ivy2"><img src="{{ asset('images/img-holly-ivy2.png') }}" alt=""/></div>
            <h2>12 Tools of Christmas</h2>
            <p><strong><u>Full Terms and Conditions </u></strong></p>
            <ol>
                <li>This Promotion is open to residents of  United Kingdom aged 18 or over, excluding employees of the Promoter, their  families, agents or anyone else professionally connected with this promotion.</li>
                <li><strong>Promotion Period:</strong> Enter between 09.30 GMT  on 11th December 2015 and 23.59 GMT on 22nd December  2015. During the Promotion Period there will be daily draws and each day starts  at 00.01 and ends at 23.59.</li>
                <li>No purchase necessary; however  internet access and a valid email address are required. </li>
                <li><strong>To Enter:</strong></li>
                <ol>
                    <li>Visit <a href="http://www.festool12tools.co.uk/" target="_blank">www.festool12tools.co.uk</a> and enter your full name, email address  and phone number to be entered into the prize draw.</li>
                    <li>If you  also enter <strong>one</strong> friend&rsquo;s name and  email address when entering you will receive <strong>one</strong> extra entry into the prize draw.</li>
                </ol>
                <li>Maximum one entry and one friend entry, per person, per day,  throughout the Promotion Period. </li>
                <li>You must ensure that you have permission from each friend to enter  their details into the entry form for the Promotion.</li>
                <li><strong>The Prizes: </strong><br>
                    <ol type="a">
                        <li><u>Daily Prizes:</u> Each day one winner (12 winners in total) will win the  nominated prize of that specific day:
                            <ul>
                                <li>1x 90  years of Festool black and stainless steel watch &amp; 1x chocolate  systainer </li>
                                <li>1x Cordless  Hammer Drill BHC 18</li>
                                <li>1x SYSLITE  DUO set</li>
                                <li>1x CTL  Midi Dust Extractor</li>
                                <li>1x  TS 55 Circular Saw</li>
                                <li>1x Cordless  Percussion Drill PDC 18/4 Li</li>
                                <li>1x Vecturo  Multi-tool</li>
                                <li>1x ETS  EC 125/3 eccentric sander </li>
                                <li>1x Cordless  pendulum jigsaw CARVEX PSC 420 Li 18</li>
                                <li>1x  HKC 55 cordless portable circular saw</li>
                                <li>1x  Joining machine Domino DF 500</li>
                                <li>1x Kapex KS 120 UG Set</li>
                            </ul>
                            <p> All products  will be 240v unless otherwise requested, subject to approval from TTS Tooltechnic  Systems GB Ltd. </p>
                        </li>
                        <li><u>Grand Prize:</u><strong> </strong>One  winner will win any tool from <a href="http://www.festool.co.uk" target="_blank">www.festool.co.uk</a> up to the value of £750.</li>
                    </ol>
                </li>
                <li>Prizes  are non-transferable, non-refundable and there is no cash alternative. </li>
                <li>In the event of unforeseen circumstances the Promoter reserves the  right to substitute the prize for an alternative of equal or greater value. </li>
                <li>Please allow up to 28 days for delivery of prizes from the date of  winner acceptance. </li>
                <li><strong>Winner Selection:</strong>
                    <ol type="a">
                        <li><u>Daily draws:</u> All valid entries received by the Promoter during the  Promotion Period will be entered into the relevant daily prize draw to be  conducted within 3 working days of each day by an independent entity.</li>
                        <li><u>Grand Prize Draw:</u> All valid entries, received by the Promoter  during the Promotion Period, will be entered into a prize draw to be conducted  within 3 working days of the closing date by an independent entity</li>
                    </ol>
                </li>
                <li><strong>Winner  Notification: </strong>Winners will  be contacted via the email address provided upon entry within 5 working days of  each draw and will be required to respond to confirm eligibility plus  acceptance of the prize within 14 days of initial contact. In the event a  winner does not respond to communications within the 14 days of initial  contact, the Promoter reserves the right to disqualify that winner and allocate  that prize to an alternate winner selected in the same manner. </li>
                <li>The Promoter reserves the right  to verify the eligibility of entrants. The Promoter may require such  information as it considers reasonably necessary for this purpose and a prize  may be withheld unless and until the Promoter is satisfied with the  verification. </li>
                <li>All entries must be made by the  entrant themselves. Bulk entries made from trade, consumer groups or third  parties will not be accepted.&nbsp;Incomplete or illegible entries, entries by  macros or other automated means (including systems which can be programmed to  enter), and entries which do not satisfy the requirements of these terms and  conditions in full will be disqualified and will not be counted. If it becomes  apparent that an entrant is using a computer(s) to circumvent this condition  by, for example, the use of &lsquo;script&rsquo;, &lsquo;brute force&rsquo;, masking their identity by  manipulating IP addresses, using identities other than their own or any other  automated means in order to increase that entrant&rsquo;s entries into the promotion  in a way that is not consistent with the spirit of the promotion, that  entrant&rsquo;s entries will be disqualified and any prize award will be void.&nbsp; </li>
                <li>No responsibility is accepted  for entries lost, damaged or delayed or as a result of any network, computer  hardware or software failure of any kind. Proof of sending will not be accepted  as proof of receipt.</li>
                <li>The Promoter will only use the  personal details supplied for the administration of the promotion and for no  other purpose, unless we have your consent. Click to see our privacy policy  here: <a href="http://www.festool.co.uk/Privacy/Pages/Privacy-Online.aspx" target="_blank">www.festool.co.uk/Privacy/Pages/Privacy-Online.aspx</a></li>
                <li>The Promoter cannot accept any  responsibility for any damage, loss or injury suffered by any entrant entering  the promotion or as a result of accepting or participating in any prize.  Nothing shall exclude the Promoter&rsquo;s liability for death or personal injury as  a result of its negligence or generally for intent as well as for gross  negligence.</li>
                <li>If for any reason the Promotion  is not capable of running as planned for reasons including but not limited to  tampering, unauthorised intervention, fraud, technical failures or any other  causes beyond the control of the Promoter which corrupt or affect the  administration, security, fairness, integrity or proper conduct of this  promotion, the Promoter reserves the right (subject to any written directions  given under applicable law) to disqualify any individual who tampers with the  entry process and to cancel, terminate, modify or suspend the promotion.</li>
                <li>If an act, omission, event or  circumstance occurs which is beyond the reasonable control of the Promoter and  which prevents the Promoter from complying with these terms and conditions the  Promoter will not be liable for any failure to perform or delay in performing  its obligation.</li>
                <li>Winners may be requested to  participate in reasonable publicity arising from the promotion. </li>
                <li>The name and county of the  winners will be available by sending a self - addressed envelope to Twelve Days  Of Christmas Prize Draw, TTS Tooltechnic Systems GB Ltd, Saxham Business Park,  Bury St Edmunds, Suffolk, IP28 6RX after 20th January 2016 for 90 days.</li>
                <li>By entering this promotion, all  participants will be deemed to have accepted and be bound by these terms and  conditions.</li>
                <li>The Promoter&rsquo;s decision is  binding in all matters relating to this promotion, and no correspondence shall  be entered into.</li>
                <li>This promotion is governed by English Law and  participants submit to the exclusive jurisdiction of the English courts.</li>
            </ol>
            <p><strong>Promoter</strong>:  TTS Tooltechnic Systems GB Ltd, Saxham Business Park,  Bury St Edmunds, Suffolk, IP28 6RX</p>
        </div>
    </section>
@endsection