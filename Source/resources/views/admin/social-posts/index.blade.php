@extends('layouts.admin')

@section('content')
    <div id="entry-list" class="media-posts panel panel-default">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-film"></i>
            Social Posts
        </div>

        <div class="row">
            <div class="col-sm-12">
                <form action="" method="get" class="form-inline panel-body filters-form">
                    <div style="padding-bottom:15px;">
                        <div class="form-group">

                        </div>

                        <div class="form-group">
                            <label for="status">Approval Status:</label>
                            {!! Form::select('status',
                            [
                            '' => 'All',
                            'approved'=> 'Approved',
                            'rejected'=> 'Rejected'
                            ],
                            $filters['status'],
                            [
                            'class' => 'form-control'
                            ])
                            !!}
                        </div>

                        <div class="form-group">

                        </div>

                        <div class="form-group">

                        </div>

                        <div class="form-group">

                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" value="search" name="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-search"></span>
                            Search
                        </button>
                        <button type="submit" value="export" name="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-cloud-download"></span>
                            Export CSV
                        </button>
                    </div>

                </form>
            </div>
        </div>

    <hr />

        <table class="table table-striped">
            <thead>
            <tr>
                <th class="col-xs-3" class="text-center">Media</th>
                <th class="col-xs-1">Network</th>
                <th class="col-xs-1">User Info</th>
                <th class="col-xs-3">Story</th>
                <th class="col-xs-1">Date</th>
                <th class="col-xs-1 text-center">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $social_posts as $row )
                <?php // var_dump($row); exit;?>
                <tr>
                    <td class="admin-media">
                        @if( $row->video_url != null)
                            <div class="embed-container">
                                <video controls>
                                    <source src="{{ $row->video_url }}" type="video/mp4"  width="400" height="200">
                                </video>
                            </div>
                        @elseif( $row->image != null )
                            <img class="submission" src="{{ $row->image }}" alt="" width="400" height="200"/>
                        @else
                            Not Applicable
                        @endif
                    </td>
                    <td>{{ $row->network }}</td>
                    <td>
                        <b>Username:</b> {{ $row->author_username }} <br/>
                        <b>Social Id:</b> {{ $row->network_id_str }}
                    </td>
                    <td>{!! nl2br((utf8_decode($row->comments))) !!}</td>
                    <td>{{ $row->post_date }}</td>
                    <td class="commands {{ $row->status == 'approved' ? 'approved':'' }} {{ $row->status == 'rejected' ? 'rejected':'' }}">
                        <span class="btn btn-success moderate-approve" id="social-post-approve" data-media="{{ $row->id }}">Approve</span>
                        <span class="btn btn-danger moderate-reject" id="social-post-reject" data-media="{{ $row->id }}">Reject</span>
                    </td>
                </tr>
            @endforeach
            @if( count($social_posts) == 0 )
                <tr>
                    <td colspan="50" class="no-matches">
                        No Matches Found
                    </td>
                </tr>
            @endif
            </tbody>
        </table>

        <div class="pagination-container">
            {!! $social_posts->appends(Input::only(['status']))->render() !!}
        </div>
    </div>
@stop
<script>
    window.onload = function() {
        // Set the size of the rendered Emojis
        // This can be set to 16x16, 36x36, or 72x72
        twemoji.size = '16x16';

        // Parse the document body and
        // insert <img> tags in place of Unicode Emojis
        twemoji.parse(document.body);


    };
</script>