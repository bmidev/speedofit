@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-file"></i>
            <strong>
                &nbsp; Metric Details
            </strong>
            <div class="pull-right">
                <a href="{{ URL::to('/metric')}}">
                    <button type="button" class="btn btn-info btn-xs"> 
                        <i class="glyphicon glyphicon-list"></i>
                        Listing
                    </button>
                </a>

                <a href="{{ URL::to('/metric/' . $response['id'] . '/edit')}}">
                    <button type="button" class="btn btn-warning btn-xs"> 
                        <i class="glyphicon glyphicon-pencil"></i>
                        Edit
                    </button>
                </a>
            </div>
        </div>
        <div class="form-group">
            {{-- ROW::START --}}
            <div class="form-group">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('metric', 'Metric:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                @if($response['metric'] === 'h')
                                    {!! 'High' !!}
                                @elseif($response['metric'] === 'm')
                                    {!! 'Medium' !!}
                                @else
                                    {!! 'Low' !!}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('value', 'Value:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! $response['value'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <div class="form-group form-inline">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('is_swimmer', 'Swimmer:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                @if($response['is_swimmer'] == 1)
                                    {!! 'Yes' !!}
                                @else
                                    {!! 'No' !!}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('is_active', 'Active:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-6">
                                @if($response['is_active'] == 1)
                                    {!! 'Yes' !!}
                                @else
                                    {!! 'No' !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            <hr>
                <div class="well">
                    <a href="{{ URL::to('/metric')}}">
                        <button type="button" class="btn btn-info btn-md"> 
                            <i class="glyphicon glyphicon-list"></i>
                            Listing
                        </button>
                    </a>

                    <a href="{{ URL::to('/metric/' . $response['id'] . '/edit')}}">
                        <button type="button" class="btn btn-warning btn-md"> 
                            <i class="glyphicon glyphicon-pencil"></i>
                            Edit
                        </button>
                    </a>
                </div>
            </div>
    </div>
@stop