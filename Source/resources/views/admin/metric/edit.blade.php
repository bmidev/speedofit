@extends('layouts.admin')

@section('content')
    <div id="entry-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-pencil"></i>
            <strong>
                &nbsp; Edit Metric
            </strong>
        </div>
        <div class="form-group">
            {!! Form::model($response, ['url' => 'metric/' . $id, 'method' => 'PUT', 'class' => 'panel-body']) !!}
            <br>
                {{-- ROW::START --}}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="col-md-2">
                                        {!! Form::label('metric', 'Metric:', ['class' => 'form-label']) !!}
                                    </div>
                                    <div class="col-md-4">
                                        @if($response['metric'] === 'h')
                                            {!! 'High' !!}
                                        @elseif($response['metric'] === 'm')
                                            {!! 'Medium' !!}
                                        @else
                                            {!! 'Low' !!}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-2">
                                        {!! Form::label('value', 'Value:', ['class' => 'form-label']) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('value', $response['value'], ['class' => 'form-control']) !!}
                                        @if ($errors->has('value'))
                                            <span class="text-danger">{!! $errors->first('value') !!}</span>
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <div class="form-group form-inline">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-2">
                                    {!! Form::label('is_swimmer', 'Swimmer:', ['class' => 'form-label']) !!}
                                </div>
                                <div class="col-md-4">
                                    @if($response['is_swimmer'] == 1)
                                        {!! 'Yes' !!}
                                    @else
                                        {!! 'No' !!}
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-2">
                                    {!! Form::label('is_active', 'Active:', ['class' => 'form-label']) !!}
                                </div>
                                <div class="col-md-6">
                                    @if($response['is_active'] == 1)
                                        {!! 'Yes' !!}
                                    @else
                                        {!! 'No' !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                <hr>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary btn-md">
                        <i class="glyphicon glyphicon-refresh"></i>
                        Update
                    </button>
                    
                    <a href="{{ URL::to('/metric')}}">
                        <button type="button" class="btn btn-default btn-md"> 
                            <i class="glyphicon glyphicon-remove"></i>
                            Cancel
                        </button>
                    </a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop