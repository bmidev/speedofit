@extends('layouts.admin')

@section('content')
    <div id="metric-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-list"></i>
            <strong>
                &nbsp; Metric Listing
            </strong>
        </div>
        <br>
        <div class="container row">
            <div class="col-sm-12">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-1">&nbsp;</th>
                            <th class="col-md-1">#</th>
                            <th class="col-md-2">Metric</th>
                            <th class="col-md-1">Value</th>
                            <th class="col-md-1">Swimmer</th>
                            <th class="col-md-1">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($responseData as $response)
                            <tr>
                                <td class="{!! ($response->is_active)? 'success': 'danger' !!}">{!! ($response->is_active)?'Active':'InActive' !!}</td>
                                <td>{!! $response->id !!}</td>
                                <td>
                                    @if($response->metric === 'h')
                                        {!! 'High' !!}
                                    @elseif($response->metric === 'm')
                                        {!! 'Medium' !!}
                                    @else
                                        {!! 'Low' !!}
                                    @endif
                                </td>
                                <td>{!! $response->value !!}</td>
                                <td>{!! ($response->is_swimmer)?'<b>Yes</b>':'No' !!}</td>
                                <td>
                                    <div class="pull-right">
                                        <a href="{{ URL::to('/metric/' . $response['id'] . '/edit')}}">
                                            <button type="button" class="btn btn-warning btn-sm"> 
                                                <i class="glyphicon glyphicon-pencil"></i>
                                                Edit
                                            </button>
                                        </a>
                                        
                                        <a href="{{ URL::to('/metric/' . $response['id'])}}">
                                            <button type="button" class="btn btn-info btn-sm"> 
                                                <i class="glyphicon glyphicon-info-sign"></i>
                                                Details
                                            </button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {!! $responseData->render() !!}
        </div>
    </div>
@stop