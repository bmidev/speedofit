@extends('layouts.admin')

@section('content')
    <div id="entry-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-plus"></i>
            <strong>
                &nbsp; Add Question
            </strong>
        </div>
        <div class="form-group">
            {!! Form::open(['url' => 'question', 'class' => 'panel-body']) !!}
                {{-- ROW::START --}}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2 text-right">
                                {!! Form::label('question', 'Question: ', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('question', '', ['class' => 'form-control']) !!}
                                @if ($errors->has('question'))
                                    <span class="text-danger">{!! $errors->first('question') !!}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <?php $g=1; ?>
                @for($i=0; $i<3; $i++)
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2 text-right">
                                    {!! Form::label('option', 'Option ' . $g . ':', ['class' => 'form-label']) !!}
                                </div>
                                <div class="col-md-8">
                                    {!! Form::text('option[' . $i . ']', '', ['class' => 'form-control']) !!}
                                    @if ($errors->has('option.' . $i))
                                        <span class="text-danger">{!! $errors->first('option.' . $i) !!}</span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    {!! Form::select('metric[' . $i . ']', ['h' => 'High', 'm' => 'Medium', 'l' => 'Low'], 'l', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $g++; ?>
                @endfor
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <div class="form-group form-inline">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2 text-right">
                                {!! Form::label('is_swimmer', 'Swimmer:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::select('is_swimmer', [1 => 'Yes', 0 => 'No'], 1, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <div class="form-group form-inline">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2 text-right">
                                {!! Form::label('is_active', 'Active:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-2">
                                {!! Form::radio('is_active', 1, true) !!}
                                {!! Form::label('is_active', 'Yes', ['class' => 'form-label']) !!}
                                </div>
                                <div class="col-md-2">
                                {!! Form::radio('is_active', 0) !!}
                                {!! Form::label('is_active', 'No', ['class' => 'form-label']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                <hr>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary btn-md">
                        <i class="glyphicon glyphicon-floppy-disk"></i>
                        Submit
                    </button>
                    
                    <a href="{{ URL::to('/question')}}">
                        <button type="button" class="btn btn-default btn-md"> 
                            <i class="glyphicon glyphicon-remove"></i>
                            Cancel
                        </button>
                    </a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop