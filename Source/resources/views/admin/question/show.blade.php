@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-file"></i>
            <strong>
                &nbsp; Question Details
            </strong>
            <div class="pull-right">
                {!! Form::open(['url' => '/question/' . $response['id'], 'id' => 'questionDelete' . $response['id'], 'method' => 'delete']) !!}
                    <a href="{{ URL::to('/question')}}">
                        <button type="button" class="btn btn-info btn-xs"> 
                            <i class="glyphicon glyphicon-list"></i>
                            Listing
                        </button>
                    </a>

                    <a href="{{ URL::to('/question/' . $response['id'] . '/edit')}}">
                        <button type="button" class="btn btn-warning btn-xs"> 
                            <i class="glyphicon glyphicon-pencil"></i>
                            Edit
                        </button>
                    </a>
                
                    <button type='button' class="btn btn-danger btn-xs deleteQuestion" id="{{$response['id']}}" data-toggle="modal" data-target="#deleteModal">
                        <i class="glyphicon glyphicon-trash"></i>
                        Delete
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="form-group">
            {{-- ROW::START --}}
            <div class="form-group">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2 text-right">
                            {!! Form::label('question', 'Question:', ['class' => 'form-label']) !!}
                        </div>
                        <div class="col-md-8">
                            {!! $response['question'] !!}
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <?php $i=1; ?>
            @foreach($response['answers'] as $option)
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2 text-right">
                            {!! Form::label('option', 'Option ' . $i . ':', ['class' => 'form-label']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! $option['answer'] !!}
                        </div>
                        <div class="col-md-2">
                            @if($option['metric'] === 'h')
                                {!! '<b>Metric:</b> High' !!}
                            @elseif($option['metric'] === 'm')
                                {!! '<b>Metric:</b> Medium' !!}
                            @else
                                {!! '<b>Metric:</b> Low' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; ?>
            @endforeach
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <div class="form-group form-inline">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2 text-right">
                            {!! Form::label('is_swimmer', 'Swimmer:', ['class' => 'form-label']) !!}
                        </div>
                        <div class="col-md-8">
                            @if($response['is_swimmer'] == 1)
                                {!! 'Yes' !!}
                            @else
                                {!! 'No' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <div class="form-group form-inline">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2 text-right">
                            {!! Form::label('is_active', 'Active:', ['class' => 'form-label']) !!}
                        </div>
                        <div class="col-md-6">
                            @if($response['is_active'] == 1)
                                {!! 'Yes' !!}
                            @else
                                {!! 'No' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            <hr>
            <div class="well">
                {!! Form::open(['url' => '/question/' . $response['id'], 'id' => 'questionDelete' . $response['id'], 'method' => 'delete']) !!}
                    <a href="{{ URL::to('/question')}}">
                        <button type="button" class="btn btn-info btn-md"> 
                            <i class="glyphicon glyphicon-list"></i>
                            Listing
                        </button>
                    </a>

                    <a href="{{ URL::to('/question/' . $response['id'] . '/edit')}}">
                        <button type="button" class="btn btn-warning btn-md"> 
                            <i class="glyphicon glyphicon-pencil"></i>
                            Edit
                        </button>
                    </a>
                
                    <button type='button' class="btn btn-danger btn-md deleteQuestion" id="{{$response['id']}}" data-toggle="modal" data-target="#deleteModal">
                        <i class="glyphicon glyphicon-trash"></i>
                        Delete
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
    {{-- [START] Delete Modal --}}
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to Delete this Question?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmDelete" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    {{-- [END] Delete Modal --}}
    
    {{-- [START] Script of Delete Modal --}}
    <script>
    $(document).ready(function(){
        $('.deleteQuestion').click(function(){
            var id = this.id;
            $('#confirmDelete').click(function(){
                $("#questionDelete" + id).submit();
            });
        });
    });
    </script>
    {{-- [END] Script of Delete Modal --}}
@stop