@extends('layouts.admin')

@section('content')
    <div id="entry-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-pencil"></i>
            <strong>
                &nbsp; Edit Question
            </strong>
        </div>
        <div class="form-group">
            {!! Form::model($response, ['url' => 'question/' . $id, 'method' => 'PUT', 'class' => 'panel-body']) !!}
            <br>
                {{-- ROW::START --}}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2 text-right">
                                {!! Form::label('question', 'Question:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('question', $response['question'], ['class' => 'form-control']) !!}
                                @if ($errors->has('question'))
                                    <span class="text-danger">{!! $errors->first('question') !!}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <?php $i=1; ?>
                @foreach($response['answers'] as $option)
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2 text-right">
                                    {!! Form::label('option', 'Option ' . $i . ':', ['class' => 'form-label']) !!}
                                </div>
                                <div class="col-md-8">
                                    {!! Form::text("option[" . $option['id'] . "][answer]", $option['answer'], ['class' => 'form-control']) !!}
                                    @if ($errors->has("option." . $option['id'] . ".answer"))
                                        <span class="text-danger">{!! $errors->first("option." . $option['id'] . ".answer") !!}</span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    {!! Form::select("option[" . $option['id'] . "][metric]", ['h' => 'High', 'm' => 'Medium', 'l' => 'Low'], $option['metric'], ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++ ?>
                @endforeach
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <div class="form-group form-inline">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2 text-right">
                                {!! Form::label('is_swimmer', 'Swimmer: ', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-8">
                                {!! Form::select('is_swimmer', [1 => 'Yes', 0 => 'No'], $response['is_swimmer'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                {{-- ROW::START --}}
                <div class="form-group form-inline">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-2 text-right">
                                {!! Form::label('is_active', 'Active: ', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-2">
                                {!! Form::radio('is_active', 1, $response['is_active']? true : false) !!}
                                {!! Form::label('is_active', 'Yes', ['class' => 'form-label']) !!}
                                </div>
                                <div class="col-md-2">
                                {!! Form::radio('is_active', 0, $response['is_active']? true : false) !!}
                                {!! Form::label('is_active', 'No', ['class' => 'form-label']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- ROW::END --}}
                <hr>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary btn-md">
                        <i class="glyphicon glyphicon-refresh"></i>
                        Update
                    </button>
                    
                    <a href="{{ URL::to('/question')}}">
                        <button type="button" class="btn btn-default btn-md"> 
                            <i class="glyphicon glyphicon-remove"></i>
                            Cancel
                        </button>
                    </a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop