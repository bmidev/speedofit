@extends('layouts.admin')

@section('content')
    <div id="metric-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-list"></i>
            <strong>
                &nbsp; User Listing
            </strong>
        </div>
        <br>
        <div class="container row">
            <div class="col-sm-12">
                <table id="userListing" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-3">Name</th>
                            <th class="col-md-3">Email</th>
                            <th class="col-md-3">Winner</th>
                            <th class="col-md-1">&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {{-- [START] Delete Modal --}}
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to Delete this Participant?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmDelete" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    {{-- [END] Delete Modal --}}
    
    {{-- [START] Winner Modal --}}
    <div class="modal fade" id="winnerModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Winner</h4>
                </div>
                <div class="modal-body">
                    <p>Mark this Participant a Winner?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmWinner" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    {{-- [END] Winner Modal --}}
    
    {{-- [START] Script of Delete & Winner Modal --}}
    <script>
    $(document).ready(function(){
        $('body').on('mouseover','.markWinner',function(){
            $('.markWinner').click(function(){
                var id = this.id;
                $('#confirmWinner').click(function(){
                    $.post({url: '/user/' + id + '/markWinner', success: function(result){
                        if(result.status){
                            $('#winnerModal').modal('toggle');
                            location.reload();
                        }
                    }});
                });
            });
        }).on('mouseout','.markWinner',function(){
            $('.markWinner').unbind('click');
        });
        
        $('body').on('mouseover','.deleteUser',function(){
            $('.deleteUser').click(function(){
                var id = this.id;
                $('#confirmDelete').click(function(){
                    $("#userDelete" + id).submit();
                });
            });
        }).on('mouseout','.deleteUser',function(){
            $('.deleteUser').unbind('click');
        });
    });
    </script>
    {{-- [END] Script of Delete & Winner Modal --}}
@stop

@section('stylesheet')

@stop

@section('scripts')
    
@stop