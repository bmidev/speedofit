<script type="text/javascript">

	$(function () {

	    $(document).ready(function () {

	    	// Build the chart
	        $('#<?php echo $data->getChartId(TRUE); ?>').highcharts({
	            chart: {
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false
	            },
	            title: {
	                text: false,
	            },
	            tooltip: {
	        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: false
	                    },
	                    showInLegend: true
	                }
	            },
	            series: [{
	                type: 'pie',
	                name: false,
	                data: [<?php echo $data->getSeriesData(); ?>]
	            }]
	        });
	    });

	});
</script>
<div id="<?php echo $data->getChartId(TRUE); ?>" class="interactive_pie_chart"></div>
