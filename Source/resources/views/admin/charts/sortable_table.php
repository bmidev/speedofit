
	<script type="text/javascript"> google.load('visualization', '1', {packages: ['table']}); </script>
	<script type="text/javascript">
	    var visualization;
	    var data;

	    var options = {'showRowNumber': true};
	    function drawVisualization()
	    {
	      // Create and populate the data table.
	      var dataAsJson =
	      {cols:[<?php echo $data->generateHead(); ?>],
	      rows:[<?php foreach ($data->getData() as $row) :
		  				?> {c:[<?php foreach ($row as $column) :
		  					?>{v:<?php if ($data->getType($column) == 'number') :
		  						?><?php echo $column; ?>,f:'<?php echo $column; ?>'<?php else:
		  						?>'<?php echo addslashes($column); ?>',<?php endif; ?>},<?php endforeach;
		  				?>]},
		  		<?php endforeach; ?>]};
	      data = new google.visualization.DataTable(dataAsJson);

	      // Set paging configuration options
	      // Note: these options are changed by the UI controls in the example.
	      options['page'] = 'enable';
	      options['showRowNumber']=false;
	      options['pageSize'] = <?php echo $data->getPerPage(); ?>;
	      options['allowHtml'] =  true;
	      options['pagingSymbols'] = {prev: 'prev', next: 'next'};
	      options['pagingButtonsConfiguration'] = 'auto';
		  options['width']=<?php echo $data->getWidth(); ?>;

	      // Create and draw the visualization.
	      visualization = new google.visualization.Table(document.getElementById('<?php echo $data->getChartId(TRUE); ?>'));
	      draw();
	    }

	    function draw() { visualization.draw(data, options); }
	    google.setOnLoadCallback(drawVisualization);
	</script>
	<div id="<?php echo $data->getChartId(TRUE); ?>" class="sortable_table"></div>
