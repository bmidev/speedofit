<script type="text/javascript">
	$(function () {
        $('#<?php echo $data->getChartId(TRUE); ?>').highcharts({
            chart: {
                zoomType: 'x',
                spacingRight: 20,
                height:240
            },
            title: {
                text: false,
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 14 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                }
            },
            yAxis: {
                title: {
                    text: false
                },
                maxPadding: 0,
            },
            tooltip: {
                shared: true
            },
            legend: {
                enabled: false
            },
            series: [{
                //type: 'area',
                name: 'Visits',
                pointInterval: 24 * 3600 * 1000,
                pointStart: Date.UTC('<?php echo $data->startYear(); ?>', '<?php echo $data->startMonth(); ?>', '<?php echo $data->startDay(); ?>'),
                data: <?php echo $data->getSeriesData(); ?>
            }]
        });
	});
</script>
<div id="<?php echo $data->getChartId(TRUE); ?>" class="line_chart"></div>