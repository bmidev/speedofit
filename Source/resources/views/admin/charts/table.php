		<?php $arr = $data->getData();
			  if (empty($arr)) : ?>

				<div class="empty">There is no data available for this time period.</div>

		<?php else: ?>
		<table cellspacing="0" id="<?php echo $data->getChartId(); ?>" class="standard_table" style="width: <?php echo $data->getWidth(); ?>;">
			<thead>
				<tr>
					<?php foreach ($data->getData() as $row) :
							foreach ($row as $key => $value) :
					?><th><?php echo $key; ?></th>
				<?php endforeach;
					  break;
					  endforeach;
				?></tr>
			</thead>
			<tbody>
			<?php $i = FALSE;
				  foreach ($data->getData() as $row) :
				?>	<tr<?php if ($i) { echo ' class="alt"'; } ?>>
				<?php foreach ($row as $key => $value) :
					?>	<td><?php echo $value; ?></td>
				<?php endforeach;
					  if ($i) { $i = FALSE; } else { $i = TRUE; }
				?></tr>
			<?php endforeach;
			?></tbody>
		</table>
		<ul class="standard_table_pages">
			<?php echo generatePageLinks($data->getPage(), $data->getTotalPages(), $data->getPrefix($form), 2); ?>

		</ul>
		<?php endif; ?>
