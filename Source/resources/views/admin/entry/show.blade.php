@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-file"></i>
            <strong>
                &nbsp; {{$response->name}} Details
            </strong>
            <div class="pull-right">
                {!! Form::open(['url' => '/entry/' . $response->id, 'id' => 'entryDelete' . $response->id, 'method' => 'delete']) !!}
                    <a href="{{ URL::to('/entry')}}">
                        <button type="button" class="btn btn-info btn-xs"> 
                            <i class="glyphicon glyphicon-list"></i>
                            Listing
                        </button>
                    </a>
                    
                    <button type='button' class="btn btn-danger btn-xs deleteEntry" id="{{$response->id}}" data-toggle="modal" data-target="#deleteModal">
                        <i class="glyphicon glyphicon-trash"></i>
                        Delete
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="form-group">
            {{-- ROW::START --}}
            <div class="form-group">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('name', 'Name:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! $response->name !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('age', 'Age:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! $response->age !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('email', 'Email:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! $response->email !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('gender', 'Gender:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                @if($response->gender == 1)
                                    {!! 'Male' !!}
                                @else
                                    {!! 'Female' !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            {{-- ROW::START --}}
            <div class="form-group form-inline">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('is_swimmer', 'Swimmer:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                @if($response->is_swimmer == 1)
                                    {!! 'Yes' !!}
                                @else
                                    {!! 'No' !!}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-2">
                                {!! Form::label('score', 'Score:', ['class' => 'form-label']) !!}
                            </div>
                            <div class="col-md-4">
                                @if($response->score > 75)
                                    {!! '<strong>' . $response->score . '% </strong>' !!}
                                @else
                                    {!! $response->score . '%' !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ROW::END --}}
            <hr>
            <div class="well">
                {!! Form::open(['url' => '/entry/' . $response->id, 'id' => 'entryDelete' . $response->id, 'method' => 'delete']) !!}
                    <a href="{{ URL::to('/entry')}}">
                        <button type="button" class="btn btn-info btn-md"> 
                            <i class="glyphicon glyphicon-list"></i>
                            Listing
                        </button>
                    </a>
                    
                    <button type='button' class="btn btn-danger btn-md deleteEntry" id="{{$response->id}}" data-toggle="modal" data-target="#deleteModal">
                        <i class="glyphicon glyphicon-trash"></i>
                        Delete
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
    {{-- [START] Delete Modal --}}
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to Delete this Participant?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmDelete" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    {{-- [END] Delete Modal --}}
    
    {{-- [START] Script of Delete Modal --}}
    <script>
    $(document).ready(function(){
        $('.deleteEntry').click(function(){
            var id = this.id;
            $('#confirmDelete').click(function(){
                $("#entryDelete" + id).submit();
            });
        });
    });
    </script>
    {{-- [END] Script of Delete Modal --}}
@stop