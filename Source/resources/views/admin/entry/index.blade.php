@extends('layouts.admin')

@section('content')
    {!! Form::hidden('user_id', $user_id, ['id' => 'user_id']) !!}
    <div id="metric-list" class="media-posts panel panel-primary">
        <div class="panel-heading">
            <i class="glyphicon glyphicon-list"></i>
            <strong>
                &nbsp; Entry Listing
                @if($user_id)
                    {{ '[User #' . $user_id . ' ]' }}
                @endif
            </strong>
        </div>
        <br>
        <div class="container row">
            <div class="col-sm-12">
                <table id="entryListing" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-1">User #</th>
                            <th class="col-md-3">Name</th>
                            <th class="col-md-1">Age</th>
                            <th class="col-md-3">Email</th>
                            <th class="col-md-1">Gender</th>
                            <th class="col-md-2">Swimmer</th>
                            <th class="col-md-2">Score</th>
                            <th class="col-md-1">&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    {{-- [START] Delete Modal --}}
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to Delete this Participant?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmDelete" class="btn btn-danger">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    {{-- [END] Delete Modal --}}
    
    {{-- [START] Script of Delete Modal --}}
    <script>
    $(document).ready(function(){
        $('body').on('mouseover','.deleteEntry',function(){
            $('.deleteEntry').click(function(){
                var id = this.id;
                $('#confirmDelete').click(function(){
                    $("#entryDelete" + id).submit();
                });
            });
        }).on('mouseout','.deleteEntry',function(){
            $('.deleteEntry').unbind('click');
        });
    });
    </script>
    {{-- [END] Script of Delete Modal --}}
@stop

@section('stylesheet')

@stop

@section('scripts')
    
@stop