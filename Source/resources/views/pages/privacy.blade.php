@extends('layouts.master')
@section('content')
<section class="common-container">
	<div class="wrapper-area">
    	<h1>Privacy</h1>
    	<p>This privacy policy has been compiled to better serve those  who are concerned with how their &lsquo;Personally identifiable information&rsquo; (PII) is  being used online. PII is information that can be used on its own or with other  information to identify, contact, or locate a single person, or to identify an  individual in context. Please read our privacy policy carefully to get a clear  understanding of how we collect, use, protect or otherwise handle your  Personally Identifiable Information in accordance with our website.</p>
    	<p> When taking the challenge on our site, you may be asked to  enter your name, age, sex and e-mail ID. You may also be asked for access to  your social networks.</p>
    	<p>We collect information only when you manually enter it on  our site.</p>
    	<p>The site is purely for entertainment purposes. We don't  collect any financial or credit card information.</p>
    	<p>You can choose to have your computer warn you each time a  cookie is being sent, or you can choose to turn off all cookies. You do this  through your browser (like Internet Explorer) settings. Each browser is a  little different, so look at your browser's Help menu to learn the correct way  to modify your cookies.</p>
    	<p>If you disable cookies off, some features will be disabled that make your site experience less efficient and some of our services will not function properly.</p>
    	<p>We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) or other third-party identifiers, compile data regarding user interactions related to our website. Primarily used to understand user experience and internal reporting.</p>
    	<p>We collect your e-mail address in order to notify you of the contest results. Any change in Privacy Policy will be updated and duly informed to relevant recipients.</p>
	</div>
</section>
@stop
@section('scripts')

@stop