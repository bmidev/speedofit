@extends('layouts.master')
@section('content')
<section class="quiz-container" id="bgVideo">
	<div class="wrapper-area">
    	{!! Form::open(['url' => 'quiz', 'class' => 'panel-body', 'id' => 'quiz-reg']) !!}
            <div id="wizard">
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <h1>I AM A</h1>
                        <p>
                            <input type="radio" name="is_swimmer" value="1" id="rdoQestion1_0" class="css-checkbox">
                            <label class="css-label" for="rdoQestion1_0">Swimmer</label>
                            
                            <input type="radio" name="is_swimmer" value="0" id="rdoQestion1_1" class="css-checkbox">
                            <label class="css-label" for="rdoQestion1_1">Non swimmer</label>
                        </p>
                        <div class="buttons"><button id="user_preference" class="next-level">Next</button></div>
                    </div>
                </section>
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <h1 id="question_0"></h1>
                        <p>
                            <input type="radio" name="option[0]" value="swimOption1" id="option0_0" class="css-checkbox">
                            <label class="css-label" id="option00" for="option0_0"></label>
                            
                            <input type="radio" name="option[0]" value="swimOption2" id="option0_1" class="css-checkbox">
                            <label class="css-label" id="option01" for="option0_1"></label>
                            
                            <input type="radio" name="option[0]" value="swimOption3" id="option0_2" class="css-checkbox">
                            <label class="css-label" id="option02" for="option0_2"></label>

                            <input type="radio" name="option[0]" value="swimOption4" id="option0_3" class="css-checkbox">
                            <label class="css-label" id="option03" for="option0_3"></label>
                        </p>
                        <div class="buttons"><button id="" class="next-level">Next</button></div>
                    </div>
                </section>
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <h1 id="question_1"></h1>
                        <p>
                            <input type="radio" name="option[1]" value="swimInWeek1" id="option1_0" class="css-checkbox">
                            <label class="css-label" id="option10" for="option1_0"></label>
                            
                            <input type="radio" name="option[1]" value="swimInWeek2" id="option1_1" class="css-checkbox">
                            <label class="css-label" id="option11" for="option1_1"></label>
                            
                            <input type="radio" name="option[1]" value="swimInWeek3" id="option1_2" class="css-checkbox">
                            <label class="css-label" id="option12" for="option1_2"></label>

                            <input type="radio" name="option[1]" value="swimInWeek4" id="option1_3" class="css-checkbox">
                            <label class="css-label" id="option13" for="option1_3"></label>
                        </p>
                        <div class="buttons"><button id="" class="next-level">Next</button></div>
                    </div>
                </section>     
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <h1 id="question_2"></h1>
                        <p>
                            <input type="radio" name="option[2]" value="swimmingFor1" id="option2_0" class="css-checkbox">
                            <label class="css-label" id="option20" for="option2_0"></label>
                            
                            <input type="radio" name="option[2]" value="swimmingFor2" id="option2_1" class="css-checkbox">
                            <label class="css-label" id="option21" for="option2_1"></label>
                            
                            <input type="radio" name="option[2]" value="swimmingFor3" id="option2_2" class="css-checkbox">
                            <label class="css-label" id="option22" for="option2_2"></label>

                            <input type="radio" name="option[2]" value="swimmingFor4" id="option2_3" class="css-checkbox">
                            <label class="css-label" id="option23" for="option2_3"></label>
                        </p>
                        <div class="buttons"><button id="" class="next-level">Next</button></div>
                    </div>
                </section>
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <h1 id="question_3"></h1>
                        <p>
                            <input type="radio" name="option[3]" value="swimmingApart1" id="option3_0" class="css-checkbox">
                            <label class="css-label" id="option30" for="option3_0"></label>
                            
                            <input type="radio" name="option[3]" value="swimmingApart2" id="option3_1" class="css-checkbox">
                            <label class="css-label" id="option31" for="option3_1"></label>
                            
                            <input type="radio" name="option[3]" value="swimmingApart3" id="option3_2" class="css-checkbox">
                            <label class="css-label" id="option32" for="option3_2"></label>

                            <input type="radio" name="option[3]" value="swimmingApart4" id="option3_3" class="css-checkbox">
                            <label class="css-label" id="option33" for="option3_3"></label>
                        </p>
                        <div class="buttons"><button id="" class="next-level">Next</button></div>
                    </div>
                </section>
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <h1 id="question_4"></h1>
                        <p>
                            <input type="radio" name="option[4]" value="swimmingApart1" id="option4_0" class="css-checkbox">
                            <label class="css-label" id="option40" for="option4_0"></label>
                            
                            <input type="radio" name="option[4]" value="swimmingApart2" id="option4_1" class="css-checkbox">
                            <label class="css-label" id="option41" for="option4_1"></label>
                            
                            <input type="radio" name="option[4]" value="swimmingApart3" id="option4_2" class="css-checkbox">
                            <label class="css-label" id="option42" for="option4_2"></label>

                            <input type="radio" name="option[4]" value="swimmingApart4" id="option4_3" class="css-checkbox">
                            <label class="css-label" id="option43" for="option4_3"></label>
                        </p>
                        <div class="buttons"><button id="" class="next-level">Next</button></div>
                    </div>
                </section>
                <h2></h2>
                <section>
                    <div class="vertical-middle">
                        <p class="personal-info">
                            I am <input name="name" type="text" id="txtName" placeholder="(Full Name)" class="txtName">, 
                            I am a <input name="age" type="text" class="txtAge" id="txtAge" placeholder="(Age)" maxlength="2"> year old <br>
                            <span>
                                <input type="radio" name="gender" value="1" id="rdoGender_0" class="css-checkbox">
                                <label class="css-label" id="option40" for="rdoGender_0">Male</label>
                                |
                                <input type="radio" name="gender" value="0" id="rdoGender_1" class="css-checkbox">
                                <label class="css-label" id="option41" for="rdoGender_1">Female</label>
                            </span>
                            participant. I can be reached <br>
                            at <input name="email" type="text" id="txtEmail" placeholder="(Email ID)" class="txtEmail">
                        </p>
                        <div class="buttons"><button class="submit-form">Let&#8217;s get going!</button></div>
                    </div>
                </section>   
            </div>
        {!! Form::close() !!}
    </div>    
</section>
@stop
@section('scripts')

@stop