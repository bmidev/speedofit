@extends('layouts.master')
@section('content')
<section class="home-banner non-mobile">
	<div><img src="{{ asset('images/banner01-2017.jpg') }}" alt=""/></div>
    {{--<div><img src="{{ asset('images/banner02.jpg') }}" alt=""/></div>
    <div><img src="{{ asset('images/banner03.jpg') }}" alt=""/></div>--}}

</section>
<section class="home-mobile-banner">
    <section class="home-intro-copy">
        <div class="wrapper-area">
            {{--<h1>Win a holiday</h1>--}}
            <h3>Thank you for participating in the Speedo Fitness Challenge.</h3>
            {{--<h3>That will make you fitter than fit!</h3>--}}
            {{--<div class="buttons"><a href={!! url('quiz') !!}>Take the challenge now</a></div>--}}
        </div> 
    </section>
</section>
@stop
@section('scripts')

@stop