@extends('layouts.master')
@section('content')
<section class="thanks-container">
    <div class="wrapper-area">
    	<h1>Thank you<br>
            <span id='participantName'>
                {!! $name !!}
            </span>
            {!! Form::hidden('participantID', $id ,['id' => 'participantID']) !!}
            {!! Form::hidden('comboID', $comboID ,['id' => 'comboID']) !!}
        </h1>
    	<div class="result-area row clear"><?php /*?><figure><img src="{{ asset('images/share-fb.jpg') }}" alt=""/></figure><?php */?>
            <div class="result col m3">
            	<h3>You are</h3>
                    <h4 class="number" id='participantScore'>{!! $score !!}%</h4>
                <h3>Speedo fit</h3>
            </div>
            <div class="copy col m9">
            	<p>
                    @if($score > 0 && $score < 40)
                        {!! 'You have a fitness level that is below average. However, it’s never too late to up your game and head towards a fitter, healthier and happier you! You could start by taking a dip!' !!}
                    @elseif($score >= 40 && $score < 60)
                        {!! 'You are not the fittest you yet, but you are on your way! Remember a healthy and fit body always gives way to a peaceful mind. It’s time to burn some calories – why don’t you start now?' !!}
                    @elseif($score >= 60 && $score < 80)
                        {!! 'You have gotten a fitness regime in place but you are yet to develop the discipline to follow it through. Why not sweat it out a little more? Dive in for a sweat-free workout, now!' !!}
                    @else
                        {!! 'Great going! You’re almost Speedo Fit! You’re one with the water and you have swum your way to a fit body! Don’t stop though, you need to keep going and with a little help from us, you are going to be fittest version of you!' !!}
                    @endif
                </p>
                <h3>DIVE NOW!</h3>
            </div>
        </div>
    	<div class="share-area clear">
            <div class="icon-send left"><img src="{{ asset('images/icon-send.gif') }}" alt=""/></div>
            <div class="copy left">
                Share your score to increase your chances of winning an <br> all-expense paid fitness trip!
            </div>
            <div class="share right">
                <a href="" class="fb" id='fb-share'><img src="{{ asset('images/icon-social-fb.png') }}" alt=""/></a> 
                <a href="javascript:;" class="tw" data-id="<?php echo $id;?>" data-percent="<?php echo $score;?>" id='tw-share'><img src="{{ asset('images/icon-social-tw.png') }}" alt=""/></a> 
            </div>
        </div>
        <h1>WHY SWIM?</h1>
        <div class="why-swim row clear">
        	<figure class="col s12 m4"><img src="{{ asset('images/why-swim01.png') }}" alt=""/><figcaption>Coolest work-out</figcaption></figure>
            <figure class="col s12 m4"><img src="{{ asset('images/why-swim02.png') }}" alt=""/><figcaption> Reduce<br>chances of Injury</figcaption></figure>
            <figure class="col s12 m4"><img src="{{ asset('images/why-swim03.png') }}" alt=""/><figcaption>Full body exercise</figcaption></figure>
            <figure class="col s12 m4 offset-m2"><img src="{{ asset('images/why-swim04.png') }}" alt=""/><figcaption>Burns 367 calories after just<br>30 minutes of breaststroke</figcaption></figure>
            <figure class="col s12 m4"><img src="{{ asset('images/why-swim05.png') }}" alt=""/><figcaption>Strengthens<br>mind and body</figcaption></figure>
        </div>
    </div>
</section>
@stop
@section('scripts')

@stop