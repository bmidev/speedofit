@extends('layouts.master')
@section('content')
<section class="common-container">
	<div class="wrapper-area">
    	<h1>ABOUT US</h1>
    	<p>The world's leading swimwear brand, we at Speedo are  passionate about life in and around the water. Our brand's heritage of  innovation derives from our leadership in competitive swimming, where more  Olympic Gold Medals have been won in Speedo than any other brand.</p>
    	<p>Building on our authentic base and using the pioneering  technology created for the planet's top swimmers, Speedo brings performance,  comfort and style to all who enter the water, whether racing for a record  finish, swimming laps for better health or making memories at the beach.</p>
    	<p>Born on Bondi Beach near Sydney, Australia in 1928. Our  history of innovation began that year, as we introduced the Racerback suit -  the world's first non-wool suit allowing greater freedom of motion. Soon after,  Swedish swimmer Arne Borg set a world record in Speedo swimwear.</p>
    	<p>Speedo's Performance Collection includes the competition and training suits that remain the number one choice of the world's fastest swimmers; Speedo's Fitness and Active Recreation Collections feature men's and women's swimwear, men's water shorts, and kids swimwear in fashion-forward styles that are designed to perform.</p>
    	<p>No matter why you dive in - training, fitness or fun - those  moments in the water are always better with Speedo.</p>
    </div>
</section>
@stop
@section('scripts')

@stop