@extends('layouts.master')
@section('content')
<section class="common-container prizes">
	<div class="wrapper-area">
    	<h1>How it works</h1>
    	<div class="how-it-works-steps row">
        	<figure class="col s12 m4"><img src="{{ asset('images/icon-how-it-works-01.png') }}" alt=""/><figcaption>Dive in and take the challenge</figcaption></figure>
            <figure class="col s12 m4"><img src="{{ asset('images/icon-how-it-works-02.png') }}" alt=""/><figcaption>Find your fitness quotient</figcaption></figure>
            <figure class="col s12 m4"><img src="{{ asset('images/icon-how-it-works-03.png') }}" alt=""/><figcaption>Share and you could win an amazing holiday</figcaption></figure>
        </div>
        <div class="buttons margin-bot"><a href={!! url('quiz') !!}>Take the challenge now</a></div>
        <h1>What&#8217;s up for grabs</h1>
        <div class="prize-container row">
            <ul class="clear">
                <li class="col s12 m5">
                    <h2>Grand <br>Prize</h2>
                    <figure><img src="{{ asset('images/img-grand-prize.png') }}" alt=""/></figure>
                </li>
                <li class="col s12 m2">&nbsp;</li>
                <li class="col s12 m5">
                    <h2>Weekly <br>Giveaways</h2>
                    <figure><img src="{{ asset('images/img-weekly-prize.png') }}" alt=""/></figure>
                </li>
            </ul>            
        </div> 
        <h1>Partners</h1>
        <div class="partners-container row">
            <ul class="clear">
                <li class="col s5 m3 offset-m2">                    
                    <figure><img src="{{ asset('images/partner-go-savvy.png') }}" alt=""/></figure>
                </li>
                <li class="col s2 m2">&nbsp;</li>
                <li class="col s5 m3">
                    <figure><img src="{{ asset('images/partner-devaaya.png') }}" alt=""/></figure>
                </li>
            </ul>            
        </div>        
    </div>
</section>
@stop
@section('scripts')

@stop