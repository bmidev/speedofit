@extends('layouts.master')
@section('content')
<section class="common-container">
	<div class="wrapper-area">
    	<h1>Terms &amp; conditions</h1>
    	<p>The participant of this contest should follow Speedo India  on Facebook and Twitter.</p>
    	<p>The participants must submit their entries only on the  microsite. Any entries posted on other social media sites would not be valid.</p>
    	<p>Participant must share their results on Facebook/ Twitter to  increase their chances of winning.</p>
    	<p>The challenge will be open from 3rd March, 2017 to 30th April 2017.</p>
    	<p>The final right to choose the winner rests with the  management of Speedo and their decision shall stand final.</p>
    	<p>The winner shall be announced during and after the contest is over. The prize will be given by Speedo India in 2 weeks after winner announcement.</p>
    	<p>Speedo India will inform the winners how to collect the  prize, after verification of their identity.</p>
    	<p>If any entry is found to have abusive and profane language,  or an attempt to attack the integrity, personality of any individual or an  entity or where there is any attempt to harass any individual or entity or  doing/having anything which in our view is demeaning or derogatory, such entry  shall stand disqualified from the contest.</p>
    	<p>Acceptance of an entry is subject to the discretion of  Speedo India.</p>
    	<p>Speedo India reserves the right to reject any entry or  disqualify any participant(s) from participating in the contest without assigning  any reason. The decision of Speedo India in this regard shall be final and  binding upon the participant(s) without any demur or objection.Gratification is  not subject to exchange or replacement or redeemable for cash or kind.</p>
    	<p>All other standard terms and conditions available at Speedo  India shall apply.</p>
    </div>
</section>
@stop
@section('scripts')

@stop