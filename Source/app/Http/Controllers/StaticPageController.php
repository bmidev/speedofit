<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class StaticPageController extends Controller {
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }
    
    /**
     * Index Page
     *
     * @return Response
     */
    public function index(){
        return view('pages.' . __FUNCTION__);
    }
    
    /**
     * Terms & Condition Page
     *
     * @return Response
     */
    public function terms(){
        return view('pages.' . __FUNCTION__);
    }
    
    /**
     * Privacy Policy Page
     *
     * @return Response
     */
    public function privacy(){
        return view('pages.' . __FUNCTION__);
    }
    
    /**
     * About Us Page
     *
     * @return Response
     */
    public function about(){
        return view('pages.' . __FUNCTION__);
    }
    
    /**
     * Prize Page
     *
     * @return Response
     */
    public function prize(){
        return view('pages.' . __FUNCTION__);
    }
}
