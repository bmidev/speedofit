<?php namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Libs\Platform\Storage\Answer\AnswerRepository;
use App\Libs\Platform\Storage\Entry\EntryRepository;
use App\Libs\Platform\Storage\Question\QuestionRepository;
use App\Libs\Platform\Storage\User\UserRepository;
use App\Libs\Platform\Storage\Metric\MetricRepository;
use App\Http\Requests\Validator\Admin\EntryValidationRequest;
use Input;
use Validator;

class ParticipantController extends Controller {
    private $answer;
    private $entry;
    private $question;
    private $metric;
    private $user;
    protected $nQuestion;

    public function __construct(AnswerRepository $answer, EntryRepository $entry, QuestionRepository $question, MetricRepository $metric, UserRepository $user){
        parent::__construct();
        
        $this->answer = $answer;
        $this->entry = $entry;
        $this->question = $question;
        $this->metric = $metric;
        $this->user = $user;
        $this->nQuestion = 5;
    }
    
    /**
     * Home Page
     *
     * @return Response
     */
    public function index(){
        return view('pages.' . __FUNCTION__);
    }
    
    /**
     * Register Page
     *
     * @return Response
     */
    public function register(){
        return view('pages.' . __FUNCTION__);
    }
    
    /**
     * Add Participant
     *
     * @return Response
     */
    public function addParticipant($userData) {        
        /* Query Creation & Fire */
        $userRegisterResponse = $this->entry->create($userData)->toArray();
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        return $userRegisterResponse;
        /* Redirect Based on Model Response */
    }
    
    /**
     * Quiz Page
     *
     * @return Response
     */
    public function quiz(){
        return view('pages.' . __FUNCTION__);
    }
    
    public function getQuestion() {
        /* Default Parameter */
        $active = false;
        $fields = ['id', 'question'];
        $isSwimmer = Input::get('is_swimmer');
        
            /* Implementing Validation */
            $dataValidator = Validator::make(Input::only('is_swimmer'), [
                'is_swimmer' => 'required|boolean'
            ]);

            if ($dataValidator->fails()) {
                $errorArray = $dataValidator->errors()->toArray();
                
                $validationLog = [];
                foreach($errorArray as $errorKey => $errorValue){
                    foreach($errorValue as $key => $value){
                        $validationLog[$errorKey] = $value;
                    }
                }

                return response(['status' => false, 'message' => $validationLog, 'data' => []]);
            }
            /* Implementing Validation */
        
        $filters['is_swimmer'] = $isSwimmer;
        $filters['allow_id'] = $this->question->getNQuestionIDs($this->nQuestion, $isSwimmer);
        $sort = ['id'];
        $with = [
            'answers' => ['id', 'question_id', 'answer']
        ];
        /* Default Parameter */
        
        /* Get 5 Question based on User Swimming Preference */
        $questionWithOption = $this->question->listing($this->nQuestion, $active, $fields, $filters, $sort, $with)->getCollection()->toArray();
        /* Get 5 Question based on User Swimming Preference */
        
        return response($questionWithOption);
    }
    
    /**
     * Calculate Result
     *
     * @return Response
     */
    public function calculateResult(){
        /* Default Variable */
        $userEmail = Input::get('email');
        $isSwimmer = Input::get('is_swimmer');
        $userData = Input::only('name', 'age', 'email', 'gender', 'is_swimmer');
        $answerSheet = Input::get('option');
        /* Default Variable */
        
        /* Implementing Validation */
        if(count($answerSheet) != $this->nQuestion){
            return redirect('/quiz')->with(['error_message' => 'Invalid Entry']);
        }
        
        foreach($answerSheet as $ans){
            if(is_null($this->answer->find($ans))){
                return redirect('/quiz')->with(['error_message' => 'Invalid Option(s) selected!']);
            }
        }
        /* Implementing Validation */
        
        /* Getting User Details */
        $userResponse = $this->user->getUserFromEmail($userEmail);
        /* Getting User Details */
        
        if(count($userResponse) > 0){
            $userData['user_id'] = $userResponse->id;
        } else {
            $newUserData['name'] = $userData['name'];
            $newUserData['email'] = $userEmail;
            
            $userResponse = $this->user->create($newUserData);
            $userData['user_id'] = $userResponse->id;
        }
        
        $entryResponse = $this->addParticipant($userData);
        
        /* Default Variable */
        $entryData['entry_id'] = $entryResponse['id'];
        $totalPoints = 0;
        /* Default Variable */
        
        /* Calculating Result */
        foreach($answerSheet as $ans){
            $answerResponse = $this->answer->find($ans);
            
            $entryData['question_id'] = $answerResponse->question_id;
            $entryData['answer_id'] = $ans;
            $entryData['metric'] = $answerResponse->metric;
            
            $metricCode = $answerResponse->metric;
            $totalPoints += $this->getPoints($isSwimmer, $metricCode);
            
            /* Updating Challenge Log */
            App::make('App\Libs\Platform\Storage\ChallengeLog\ChallengeLogRepository')->create($entryData);
            /* Updating Challenge Log */
        }
        /* Calculating Result */
        
        $finalScore = (int)$totalPoints / $this->nQuestion; // Final Score
        
        /* Updating Score */
        $this->entry->update($entryData['entry_id'], ['score' => $finalScore]);
        /* Updating Score */
        
        /* Building Image */
        $idCombo = $userResponse->id . 'S' . $entryData['entry_id'];
        $this->buildShareImage($idCombo, $userResponse->name, $finalScore);
        
        return view('pages.acknowledge')->with(['id' => $entryResponse['user_id'], 'comboID' => $idCombo, 'name' => $entryResponse['name'], 'score' => $finalScore]);
    }
    
    public function getPoints($isSwimmer, $metricCode) {
        return $this->metric->getMetricValue($isSwimmer, $metricCode)->toArray()['value'];
    }
    
    public function buildShareImage($userID, $rawUserName, $rawUserScore) {
        // Create Image From Existing File
        $shareImage = imagecreatefromjpeg('images/share-fb.jpg');
        
        // Allocate A Color For The Text
        $white = imagecolorallocate($shareImage, 255, 255, 255);
        
        // Set Path to Font File
        $fontPath = 'fonts/RalewayHeavy.ttf';
        
        // Set User Name On Image
        $userName = mb_strtoupper($rawUserName, 'UTF-8');
        
        // get image dimensions
        list($imgWidth, $imgHeight,,) = getimagesize('images/share-fb.jpg');
        
        $fontSize = 40;
        $p = imagettfbbox($fontSize, 0, $fontPath, $userName);
        $txtWidth = $p[2] - $p[0];
        
        // now center the text
        $y = $imgHeight * 0.28; // baseline of text at 30% of $imgHeight
        $x = ($imgWidth - $txtWidth) / 2;
        
        imagettftext($shareImage, $fontSize, 0, $x, $y, $white, $fontPath, $userName);
        
        // Set User Score On Image
        $userScore = $rawUserScore . '%';
        imagettftext($shareImage, 75, 0, 210, 410, $white, $fontPath, $userScore);
        
        // Saving Generated Image
        $fileName = 'images/user/' . $userID . '.jpg';
        imagejpeg($shareImage, $fileName, 100);
        
        // Free up memory
        imagedestroy($shareImage);
    }
    
    /**
     * Acknowledge Page
     *
     * @return Response
     */
    public function acknowledge(){
        return view('pages.' . __FUNCTION__);
    }
    
    public function validateData() {
        /* Default Variable */
        $userData = Input::only('name', 'age', 'email', 'gender', 'is_swimmer');
        /* Default Variable */
        
        /* Implementing Validation */
        $entryValidationRequest = new EntryValidationRequest;
        $entryValidator = Validator::make($userData, $entryValidationRequest->rules(), $entryValidationRequest->messages());
        
        if ($entryValidator->fails()) {
            $errorArray = $entryValidator->errors()->toArray();
            
            $validationLog = '';
            foreach($errorArray as $errorKey => $errorValue){
                foreach($errorValue as $key => $value){
                    $validationLog = $value;
                }
            }
            
            return response(['status' => false, 'message' => $validationLog, 'data' => []]);
        }
        return response(['status' => true, 'message' => 'Go Ahead', 'data' => []]);
        /* Implementing Validation */
    }
}
