<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Step1Request;
use App\Http\Requests\Step2Request;
use App\Http\Requests\InviteRequest;
use App;
use Illuminate\Support\Facades\Session;
use Mail;

class ActionController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function step1(Step1Request $request)
    {
        $now = date('Y-m-d H:i:s');
        $today = date('Y-m-d');

        $prize_id = $request->input('prize_id');
        Session::put('prize.id', $prize_id);

        $email = $request->input('email');
        $user = App\User::where('email', $email)->first();
        //if user exist
        if (isset($user->id)) {
            // check whether user have an entry with the prize id
            $entry = App\Entry::where('user_id', $user->id)->where('prize_id', $prize_id)->first();
            if (isset($entry->id)) {
                // display Sorry
                return response()->json([
                    'success' => true,
                    'message' => 'sorry page',
                    'view' => view('partials.sorry')->render()
                ]);
            }
            // check whether the prize is still available
            $prize_today = App\Prize::where('id', $prize_id)->where('unlock_date', $today)->first();
            if ($prize_today == null) {
                // display error Prize expired. Please refresh the page.
                return response()->json([
                    'success' => false,
                    'message' => 'Prize expired'
                ], 403);
            }

            // save entry
            $inputData = array(
                'user_id' => $user->id,
                'prize_id' => $prize_id,
                'is_mobile' => IS_MOBILE == false ? 0 : 1,
                'ip_address' => $this->get_ip(),
                'agent' => $this->data['agent']->device() . ';' . $this->data['agent']->platform() . ';' . $this->data['agent']->browser()
            );
            $entry_save = App\Entry::create($inputData);

            $user_name = $user->first_name . ' ' . $user->last_name;
            Session::put('user.id', $user->id);
            Session::put('user.name', $user_name);
            Session::put('user.email', $user->email);
            Session::put('user.entry_id', $entry_save->id);
            // show invite screen
            return response()->json([
                'success' => true,
                'message' => 'invite screen',
                'view' => view('partials.thankyou-invite')->render()
            ]);
        } else {
            Session::put('user.email', $email);
//            dd($email);
            return response()->json([
                'success' => true,
                'message' => 'register',
                'view' => view('partials.register')->render()
            ]);
        }
    }

    public function step2(Step2Request $request)
    {
        $now = date('Y-m-d H:i:s');
        $today = date('Y-m-d');

        if (!Session::has('user.email') || !Session::has('prize.id')) {
            return response()->json([
                'success' => false,
                'message' => 'session expired'
            ], 403);
        }

        // check whether user exist
        $user = App\User::where('email', Session::get('user.email'))->first();
        if (isset($user->id)) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the email id is taken.'
            ], 403);
        }
        // check whether the prize is still available
        $prize_today = App\Prize::where('id', Session::get('prize.id'))->where('unlock_date', $today)->first();
        if ($prize_today == null) {
            return response()->json([
                'success' => false,
                'message' => 'prize expired'
            ], 403);
        }

        // save the user
        $inputDataUser = array(
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => Session::get('user.email'),
            'phone' => $request->input('phone'),
            'over_18' => 1,
            'tnc' => 1,
            'optin' => $request->input('optin') == 'on' ? 1 : 0,
            'is_mobile' => IS_MOBILE == false ? 0 : 1,
            'ip_address' => $this->get_ip(),
            'agent' => $this->data['agent']->device() . ';' . $this->data['agent']->platform() . ';' . $this->data['agent']->browser()
        );

        $user_save = App\User::create($inputDataUser);

        // save the entry
        $inputDataEntry = array(
            'user_id' => $user_save->id,
            'prize_id' => $prize_today->id,
            'is_mobile' => IS_MOBILE == false ? 0 : 1,
            'ip_address' => $this->get_ip(),
            'agent' => $this->data['agent']->device() . ';' . $this->data['agent']->platform() . ';' . $this->data['agent']->browser()
        );

        $entry_save = App\Entry::create($inputDataEntry);
        $user_name = $user_save->first_name . ' ' . $user_save->last_name;

        Session::put('user.id', $user_save->id);
        Session::put('user.name', $user_name);
        Session::put('user.email', $user_save->email);
        Session::put('user.entry_id', $entry_save->id);

        return response()->json([
            'success' => true,
            'message' => 'invite screen',
            'view' => view('partials.thankyou-invite')->render()
        ]);

    }

    public function invite(InviteRequest $request)
    {
        $now = date('Y-m-d H:i:s');
        $today = date('Y-m-d');

        if (!Session::has('user.id') || !Session::has('prize.id') || !Session::has('user.entry_id')) {
            return response()->json([
                'success' => false,
                'message' => 'Session expired'
            ], 403);
        }
        $invite_email = $request->input('email');
        if (Session::get('user.email') == $invite_email) {
            return response()->json([
                'success' => false,
                'message' => 'You cannot send invitation to your own email'
            ], 403);
        }
        $inputDataInvites = array(
            'user_id' => Session::get('user.id'),
            'name' => $request->input('name'),
            'email' => $invite_email,
            'entry_id' => Session::get('user.entry_id')
        );

        // Todo send mail
        $mail_data = array(
            'invite_data' => $inputDataInvites,
//            'prizes_data' => $this->data['all_prizes'],
            'inviter' => Session::get('user.name')
        );
        $mail_sent = $this->send_email($mail_data);

        if(!$mail_sent){
            return response()->json([
                'success' => false,
                'message' => 'Failure in sending invitation. Please try again.'
            ], 403);
        }
        $inputDataInvites['mail_sent'] = 1;

        $invite = App\Invite::create($inputDataInvites);

        $inputDataEntry = array(
            'user_id' => Session::get('user.id'),
            'prize_id' => Session::get('prize.id'),
            'is_mobile' => IS_MOBILE == false ? 0 : 1,
            'ip_address' => $this->get_ip(),
            'agent' => $this->data['agent']->device() . ';' . $this->data['agent']->platform() . ';' . $this->data['agent']->browser(),
            'invited' => 1
        );
        $entry_save = App\Entry::create($inputDataEntry);

        Session::forget('user');
        Session::forget('prize');

        return response()->json([
            'success' => true,
            'message' => 'thank you',
            'view' => view('partials.thankyou')->render()
        ]);
    }

    private function send_email($data)
    {
//        dd($data);
        Mail::send('emails.invite', $data, function ($message) use ($data) {
            $message->to($data['invite_data']['email'])->subject('Festool 12 Tools of Christmas, enter now for your chance to win!');
        });
//        if (count(Mail::failures()) > 0) {
//            return false;
//        }
        return true;
    }

    private function get_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
