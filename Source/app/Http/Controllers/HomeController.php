<?php namespace App\Http\Controllers;

use App;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
        $this->test();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

	public function test(){
		$prize = App\Prize::find(1);
        $user = App\User::find(1);

        foreach($user->prizes as $row){
            var_dump($row->name, $row->pivot->winner);
        }
        echo '========================';
        foreach ($prize->users as $row){
            var_dump($row->first_name, $row->last_name, $row->pivot->winner);
        }
        exit;
        dd($user->prizes);
	}

}
