<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\SocialPost;
use DB;

class SocialPostsController extends Controller
{

    /*
    |---------------------------------------------------------------------------
    | Admin Entries Controller
    |---------------------------------------------------------------------------
    |
    | Primary admin controller displaying entry stats
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    public function index(Request $request){
        $take = 100;
        if(isset($_GET['page'])){
            $page = (int) $_GET['page'];
            $skip = ($page - 1) * $take;
            if($page > 1){
                $social_posts = SocialPost::orderBy('created_at', 'desc')->take($take)->skip($skip);
            } else {
                $social_posts = SocialPost::orderBy('created_at', 'desc')->take($take);
            }

        } else {
            $social_posts = SocialPost::orderBy('created_at', 'desc')->take($take);
        }

        if(isset($_GET['status'])){
            switch($request->input('status', null)) {
                case "approved":
//                echo 'here';
                    $social_posts = $social_posts->where('status', 'approved');

                    break;
                case "rejected":
                    $social_posts = $social_posts->where('status', 'rejected');
                    break;
            }
        }


        // Export Options
        $submitAction = $request->input('submit');
        if( $submitAction == "export" ) {
            $posts = $social_posts->get()->toArray();
            return $this->export($posts);
        }

        $social_posts = $social_posts->paginate(20);


        return view('admin.social-posts.index', [
            'social_posts' => $social_posts,
            'filters' => [
                'status' 			=> $request->input('status', null),
            ]
        ]);
    }


    /**
     * Export posts using the same search filtering from the index action
     */
    private function export($posts) {
        $headers = [
            'Content-type'        => 'application/csv',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=State_Farm_Social_' . date('Y-m-d_H-i') . '.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        # add headers for each column in the CSV download
        array_unshift($posts, array_keys($posts[0]));

        $callback = function() use ($posts) {
            $FH = fopen('php://output', 'w');
            foreach ($posts as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Response::stream($callback, 200, $headers);
    }
}