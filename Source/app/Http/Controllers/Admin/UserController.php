<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libs\Platform\Storage\User\UserRepository;

class UserController extends Controller {
    private $user;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user){
        parent::__construct();
        
        $this->user = $user;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        /* HTML View Response */
        return view('admin.user.' . __FUNCTION__);
        /* HTML View Response */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        /* Default Variables */
        $fields = [];
        $with = [];
        /* Default Variables */
        
        /* Get User */
        $response = $this->user->view($id, $fields, $with);
        /* Get User & Options */
        
        /* HTML View Response */
        return view('admin.user.' . __FUNCTION__)->with(['response' => $response]);
        /* HTML View Response */
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id){
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        /* Query Creation & Fire */
        $this->user->delete($id);
        /* Query Creation & Fire */

        /* Redirect Based on Model Response */
        return redirect('/user')->with(['message' => 'User Deleted!']);
        /* Redirect Based on Model Response */
    }
    
    /**
     * Mark participant as Winner.
     *
     * @param  int  $user_id
     * @return Response
     */
    public function markWinner($user_id){
        /* Separation & Limitations of Data By Models */
        $data['is_winner'] = 1;
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->user->update($user_id, $data);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            return response(['status' => true, 'message' => 'Participant marked as Winner', 'data' => []]);
        } else {
            return response(['status' => false, 'message' => 'Oops! Something went wrong', 'data' => []]);
        }
        /* Redirect Based on Model Response */
    }
    
    public function datatables() {
        /* Query Creation & Fire */
        return $this->user->datatables();
        /* Query Creation & Fire */
    }
}
