<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libs\Platform\Storage\Entry\EntryRepository;
use App\Libs\Platform\Storage\User\UserRepository;

class EntryController extends Controller {
    private $entry;
    private $user;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EntryRepository $entry, UserRepository $user){
        parent::__construct();
        
        $this->entry = $entry;
        $this->user = $user;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($user_id = 0){
        /* HTML View Response */
        return view('admin.entry.' . __FUNCTION__)->with(['user_id' => $user_id]);
        /* HTML View Response */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        /* Default Variables */
        $fields = [];
        $with = [];
        /* Default Variables */
        
        /* Get Entry */
        $response = $this->entry->view($id, $fields, $with);
        /* Get Entry & Options */
        
        /* HTML View Response */
        return view('admin.entry.' . __FUNCTION__)->with(['response' => $response]);
        /* HTML View Response */
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id){
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        /* Query Creation & Fire */
        $this->entry->delete($id);
        /* Query Creation & Fire */

        /* Redirect Based on Model Response */
        return redirect('/entry')->with(['message' => 'User Deleted!']);
        /* Redirect Based on Model Response */
    }
    
    public function datatables($userID) {
        /* Query Creation & Fire */
        return $this->entry->datatables($userID);
        /* Query Creation & Fire */
    }
}
