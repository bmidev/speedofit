<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Brandmovers\Charts\LineChart;
use App\Brandmovers\Charts\PieChart;
use App\Brandmovers\Charts\Stats;
use App\Brandmovers\Ga\Model\GaModel;

class DashboardController extends Controller {
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Admin dashboard
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request){
        $app_start_date = date('m/d/Y', Config::get('admin.app_start_date'));
        
        if(Config::get('admin.app_end_date') > time()){
            $app_end_date = date('m/d/Y');
        } else {
            $app_end_date = date('m/d/Y', Config::get('admin.app_end_date'));
        }
        
        $startDate = $request->input('start-date', $app_start_date);
        $endDate = $request->input('end-date', $app_end_date);
        
        $startDate = date_create_from_format('m/d/Y', $startDate);
        $endDate = date_create_from_format('m/d/Y', $endDate);
        
        $this->gaModel = new GaModel(Config::get('admin.ga_profile_id'));
        
        $options = array('chartId' => 'visitors', 'width' => '960');
        
        $start_date = $startDate->getTimeStamp();
        $end_date = $endDate->getTimeStamp();
        
        $this->db_analytics($start_date, $end_date);
        
        $visitors = new LineChart($this->gaModel->dailyVisits($start_date, $end_date), $options, $start_date, $end_date);
        
        $basic = new Stats($this->gaModel->basicStats($start_date, $end_date));
        
        $colors = array('#5B5C77', '#D9D676', '#90D669', '#8EAC4F', '#C98956', '#1C82D8');
        
        // Pie Charts
        $options_mobile = array('width' => 280, 'height' => 180, 'chart_id' => 'mobile_pie_chart', 'colors' => $colors);
        $options_channels = array('width' => 280, 'height' => 180, 'chartId' => 'channel_pie_chart', 'colors' => $colors);
        $mobile = new PieChart($this->gaModel->mobileVisitors($start_date, $end_date, 5), $options_mobile);
        $channels = new PieChart($this->gaModel->getVisitorsByChannel($start_date, $end_date, 5), $options_channels);
        $gaEvents = $this->gaModel->getEvents($start_date, $end_date);
        $fbShares = isset($gaEvents['share']['share facebook']['Facebook Share']) ? $gaEvents['share']['share facebook']['Facebook Share'] : 0;
        $twShares = isset($gaEvents['share']['share twitter']['Twitter Share']) ? $gaEvents['share']['share twitter']['Twitter Share'] : 0;
//        $ind = isset($gaEvents['share']['share twitter']['6']) ? $gaEvents['share']['share twitter']['6'] : 0;
        
        $this->db_analytics($start_date, $end_date);
        
        return view('admin.dashboard.index', [
                    'filters' => [
                        'start-date' => $request->input('start-date', $app_start_date),
                        'end-date' => $request->input('end-date', $app_end_date) // Use Request Data since we actually add 1 to end date since we're checking for less than
                    ],
                    'visitors' => $visitors,
                    'basic' => $basic,
                    'mobile' => $mobile,
                    'channels' => $channels,
                    'fbShares' => $fbShares + 59,
                    'twShares' => $twShares,
                    'entries_count' => $this->data['entries_count'],
                    'unique_entries' => $this->data['unique_entries'],
             ]);
    }

    private function db_analytics($start_date, $end_date){
        $start_time = date('Y-m-d 00:00:00', $start_date);
        $end_time = date('Y-m-d 23:59:59', $end_date);
        $this->data['entries_count'] = \App\Models\Entry::where('created_at', '>=', $start_time)->where('created_at', '<=', $end_time)->count();
        $this->data['unique_entries'] = \App\Models\User::where('created_at', '>=', $start_time)->where('created_at', '<=', $end_time)->count();
    }

}
