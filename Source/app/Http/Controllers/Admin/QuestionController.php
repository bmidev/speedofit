<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Validator\Admin\QuestionValidationRequest;
use App\Libs\Platform\Storage\Question\QuestionRepository;
use App\Libs\Platform\Storage\Answer\AnswerRepository;
use Input;

class QuestionController extends Controller {
    private $question;
    private $answer;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(QuestionRepository $question, AnswerRepository $answer){
        parent::__construct();
        
        $this->question = $question;
        $this->answer = $answer;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        /* HTML View Response */
        return view('admin.question.' . __FUNCTION__);
        /* HTML View Response */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return view('admin.question.' . __FUNCTION__);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(QuestionValidationRequest $questionValidator){
        /* Separation & Limitations of Data By Models */
        $data['questionEntry'] = Input::only('question', 'is_swimmer','is_active');
        $data['optionEntry'] = Input::only('option', 'metric');
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->question->create($data['questionEntry']);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            /* Query Creation & Fire */
            $data['optionEntry']['question_id'] = $mr->id;
            $this->answer->create($data['optionEntry']);
            /* Query Creation & Fire */
            
            return redirect('question/' . $mr->id)->with(['message' => 'Question Added']);
        }
        /* Redirect Based on Model Response */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        /* Default Variables */
        $active = false;
        $fields = [];
        $with = [
            'answers' => ['*']
        ];
        /* Default Variables */
        
        /* Get Question */
        $response = $this->question->view($id, $active, $fields, $with)->toArray();
        /* Get Question & Options */
        
        /* HTML View Response */
        return view('admin.question.' . __FUNCTION__)->with(['response' => $response]);
        /* HTML View Response */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        /* Default Variables */
        $active = false;
        $fields = [];
        $with = [
            'answers' => ['*']
        ];
        /* Default Variables */
        
        /* Get Question with Option */
        $response = $this->question->view($id, $active, $fields, $with)->toArray();
        /* Get Question with Option */
        
        return view('admin.question.' . __FUNCTION__)->with(['id' => $id, 'response' => $response]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(QuestionValidationRequest $questionValidator, $id){
        /* Separation & Limitations of Data By Models */
        $data['questionEntry'] = Input::only('question', 'is_swimmer','is_active');
        $data['optionEntry'] = Input::only('option');
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->question->update($id, $data['questionEntry']);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            /* Query Creation & Fire */
            foreach($data['optionEntry']['option'] as $optionKey => $optionValue){
                $this->answer->update($optionKey, $optionValue);
            }
            /* Query Creation & Fire */
            
            return redirect('/question/' . $id)->with(['message' => 'Question Updated']);
        }
        /* Redirect Based on Model Response */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        /* Query Creation & Fire */
        $this->question->delete($id);
        /* Query Creation & Fire */

        /* Redirect Based on Model Response */
        return redirect('/question')->with(['message' => 'Question Deleted!']);
        /* Redirect Based on Model Response */
    }
    
    public function datatables() {
        /* Query Creation & Fire */
        return $this->question->datatables();
        /* Query Creation & Fire */
    }
}
