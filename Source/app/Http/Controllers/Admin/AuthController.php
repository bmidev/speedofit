<?php namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Admin Login
     *
     * @return Response
     */
    public function getIndex(){
        if( !Auth::guest() ) {
            return redirect('/dashboard');
        }

        return view('admin.login', []);
    }


    /**
     * Process Admin Login
     * 
     * @param Request $request
     * @return Response
     */
    public function postIndex(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');
        
        if( Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->intended('dashboard');
        }

        return redirect()->back()->withInput()->withErrors('Invalid Login.');
    }

    /**
     * Logout of Admin
     */
    public function getLogout(){
        Auth::logout();
        
        return redirect('/admin');
    }
}



		