<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Validator\Admin\MetricValidationRequest;
use App\Libs\Platform\Storage\Metric\MetricRepository;
use Input;

class MetricController extends Controller {
    private $metric;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MetricRepository $metric){
        parent::__construct();
        
        $this->metric = $metric;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        /* Default Variables */
        $active = 0;
        $fields = [];
        $filters = [];
        $limit = 25;
        $sort = ['id'];
        $with = [];
        /* Default Variables */
        
        /* Get Data for View */
        $response = $this->metric->listing($limit, $active, $fields, $filters, $sort, $with);
        /* Get Data for View */
        
        /* HTML View Response */
        return view('admin.metric.' . __FUNCTION__)->with(['responseData' => $response]);
        /* HTML View Response */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(){
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        /* Default Variables */
        $active = false;
        $fields = [];
        $with = [];
        /* Default Variables */
        
        /* Get Metric */
        $metricResponse = $this->metric->view($id, $active, $fields, $with)->toArray();
        /* Get Metric & Options */
        
        /* HTML View Response */
        return view('admin.metric.' . __FUNCTION__)->with(['response' => $metricResponse]);
        /* HTML View Response */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        /* Default Variables */
        $active = false;
        $fields = [];
        $with = [];
        /* Default Variables */
        
        /* Get Metric */
        $metricResponse = $this->metric->view($id, $active, $fields, $with)->toArray();
        /* Get Metric */
        
        return view('admin.metric.' . __FUNCTION__)->with(['id' => $id, 'response' => $metricResponse]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(MetricValidationRequest $metricValidator, $id){
        /* Separation & Limitations of Data By Models */
        $data = Input::only('value');
        /* Separation & Limitations of Data By Models */
        
        /* Query Creation & Fire */
        $mr = $this->metric->update($id, $data);
        /* Query Creation & Fire */
        
        /* Redirect Based on Model Response */
        if ($mr) { // If Successful
            return redirect('/metric/' . $id)->with(['message' => 'Metric Updated']);
        }
        /* Redirect Based on Model Response */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id){
        return redirect()->back();
    }
}
