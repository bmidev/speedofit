<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Session;

class TestController extends Controller {


	public function __construct()
	{
		parent::__construct();
		$this->middleware('test');
        $this->initial_setup();
	}

    public function initial_setup(){
        if(Session::has('prize')){
            Session::forget('prize');
        }
        if(Session::has('user')){
            Session::forget('user');
        }
        $today = date('Y-m-d');
        $this->data['prize_today'] = App\Prize::where('unlock_date', $today)->first();


    }

	public function getLogin(){
		return view('modals.login', $this->data);
	}

	public function getInvite(){
		$mail_data = array(
//            'invite_data' => $inputDataInvites,
            'prizes_data' => $this->data['all_prizes']
        );
		return view('emails.invite', $mail_data);
	}

}
