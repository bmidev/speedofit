<?php

/* [START] Participant Routes */
get('/', 'StaticPageController@index');
get('/storelocator', function() { return view('pages.storelocator'); });
get('about-us', 'StaticPageController@about');
get('privacy-policy', 'StaticPageController@privacy');
//get('prizes', 'StaticPageController@prize');
get('terms-and-condition', 'StaticPageController@terms');

//get('register', 'ParticipantController@register');
//post('register', 'ParticipantController@addParticipant');
//get('quiz', 'ParticipantController@quiz');
//get('fetch-question', 'ParticipantController@getQuestion');
//post('quiz', 'ParticipantController@calculateResult');
//post('validate-data', 'ParticipantController@validateData');
//get('result', 'ParticipantController@acknowledge');
/* [END] Participant Routes */

/* [START] Admin Routes */
Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function() {
    get('dashboard', 'DashboardController@index');
    
    Route::resource('entry', 'EntryController');
    post('user/{user_id}/markWinner', 'UserController@markWinner')->where(['user_id' => '[0-9]+']);
    Route::resource('question', 'QuestionController');
    Route::resource('metric', 'MetricController');
    Route::resource('user', 'UserController');
    get('user/{user_id}/entries', 'EntryController@index')->where(['user_id' => '[0-9]+']);
    
    Route::post('datatable/entryProcessing/{user_id}', 'EntryController@datatables')->where(['user_id' => '[0-9]+']);
    Route::post('datatable/questionProcessing', 'QuestionController@datatables');
    Route::post('datatable/userProcessing', 'UserController@datatables');
});

Route::group(['namespace' => 'Admin'], function() {
    Route::controller('admin', 'AuthController');
});
/* [END] Admin Routes */