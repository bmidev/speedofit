<?php namespace App\Http\Requests\Validator\Admin;

use App\Http\Requests\Request;
use Illuminate\Http\Request as HttpRequest;

class QuestionValidationRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(HttpRequest $request){
        $rules = [
            'question' => 'Required'
        ];
        
        foreach($this->request->get('option') as $optionKey => $optionValue){
            $rules['option.'.$optionKey] = 'required';
        }
        
        if($request->isMethod('PUT')){ // If form action is PUT i.e. Edit
            foreach($this->request->get('option') as $optionKey => $optionValue){
                $rules['option.'.$optionKey . '.answer'] = 'required';
            }
        }
        
        return $rules;
    }

    public function messages(){
        $message = [
            'question.required' => 'Question is required'
        ];
        
        foreach($this->request->get('option') as $optionKey => $optionValue){
            $message['option.'. $optionKey . '.required'] = 'Option ' . ($optionKey+1) . ' is Required';
        }
        
        if(Request::isMethod('PUT')){ // If form action is PUT i.e. Edit
            foreach($this->request->get('option') as $optionKey => $optionValue){
                $message['option.'. $optionKey . '.answer.required'] = 'Option is Required';
            }
        }
        
        return $message;
    }
}
