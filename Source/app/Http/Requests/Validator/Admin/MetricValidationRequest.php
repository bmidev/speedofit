<?php namespace App\Http\Requests\Validator\Admin;

use App\Http\Requests\Request;

class MetricValidationRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'value' => 'Required|Integer'
        ];
    }

    public function messages(){
        return [
            'value.required' => 'Value is required',
            'value.integer' => 'Value must be a number',
        ];
    }
}
