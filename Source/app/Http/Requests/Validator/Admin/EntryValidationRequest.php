<?php namespace App\Http\Requests\Validator\Admin;

use App\Http\Requests\Request;

class EntryValidationRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'name' => 'Required|Regex:/^[a-zA-Z\s]+$/i|Max:100',
            'age' => 'Required|Integer|min:13',
            'email' => 'Required|email',
            'gender' => 'Required',
            'is_swimmer' => 'Required|Boolean'
        ];
    }
    
    public function messages(){
        return [
            'name.required' => 'Please enter your name',
            'name.regex' => 'Allowed characters for Name are a-z (lowercase and capital) and space ( )',
            'name.max' => 'Name cannot exceed 100 characters',
            'age.required' => 'Please enter your age',
            'age.integer' => 'Invalid Age',
            'email.required' => 'Please enter an email',
            'email.email' => 'Email you entered is not valid',
            'gender.required' => 'Please select your gender',
            'is_swimmer.required' => 'Please select whether you\'re Swimmer or Non Swimmer',
            'is_swimmer.boolean' => 'Invalid Selection'
        ];
    }
}
