<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class InviteRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:128',
//			'email' => 'required|email|unique:users|max:128'
			'email' => 'required|email|unique:invites|max:128'
		];
	}

	public function messages()
	{
		return [
			'email.unique' => "This :attribute address has already received an invitation."
		];
	}

}
