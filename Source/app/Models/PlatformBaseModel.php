<?php namespace App\Models;

class PlatformBaseModel extends \Eloquent {
    /**
     * Method to ensure that the array contains keyvalue pair where key is
     * of type 'string'
     * 
     * @param array $array
     * @return boolean
     */
    private function is_assoc($array) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

    /**
     * Boot method
     */
    public static function boot() {
        parent::boot();
    }

    /**
     * Method to process the 'selects' (fields) associated with the 'with'
     * 
     * @param type $relations
     * @return type
     */

    public function processWithSelects($relations) {
        if ($this->is_assoc($relations)) {
            foreach ($relations as $key=>$value) {
                if (is_array($value)) {
                    $relations[$key] = function($query) use ($value) {
                        return $query->select($value);
                    };
                }
            }
        }
        
        return $relations;
    }
}