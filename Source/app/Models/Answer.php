<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends PlatformBaseModel{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $fillable = ['question_id', 'answer', 'metric'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $softDelete = true;
    protected $table = 'answers';

    /* Relationship Methods */
    public function question(){
        return $this->belongsTo('App\Models\Question');
    }
    /* Relationship Methods */
}
