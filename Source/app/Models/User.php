<?php namespace App\Models;

class User extends PlatformBaseModel{
    
    protected $fillable = ['name', 'email', 'is_winner'];
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'users';
}
