<?php namespace App\Models;

class ChallengeLog extends PlatformBaseModel{
    
    protected $fillable = ['entry_id', 'question_id', 'answer_id', 'metric'];
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'challenge_logs';
}
