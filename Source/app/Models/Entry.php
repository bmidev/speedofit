<?php namespace App\Models;

class Entry extends PlatformBaseModel{
    
    protected $fillable = ['user_id', 'name', 'age', 'email', 'gender', 'score', 'is_swimmer'];
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $softDelete = true;
    protected $table = 'entries';
    
    /* Relationship Methods */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    /* Relationship Methods */
}
