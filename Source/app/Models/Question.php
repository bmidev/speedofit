<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends PlatformBaseModel{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $fillable = ['question', 'is_swimmer', 'is_active'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $softDelete = true;
    protected $table = 'questions';
    
    /* Relationship Methods */
    public function answers(){
        return $this->hasMany('App\Models\Answer');
    }
    /* Relationship Methods */
}
