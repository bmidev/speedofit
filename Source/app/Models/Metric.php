<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Metric extends PlatformBaseModel{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $fillable = ['value'];
    protected $guarded = ['metric', 'is_swimmer', 'is_active'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $softDelete = true;
    protected $table = 'metrics';
}
