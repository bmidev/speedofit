<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot(){
            
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register(){
            $this->app->bind(
                'Illuminate\Contracts\Auth\Registrar',
                'App\Services\Registrar'
            );
            
            $this->app->bind(
                'App\Libs\Platform\Storage\Answer\AnswerRepository',
                'App\Libs\Platform\Storage\Answer\EloquentAnswerRepository'
            );
            
            $this->app->bind(
                'App\Libs\Platform\Storage\ChallengeLog\ChallengeLogRepository',
                'App\Libs\Platform\Storage\ChallengeLog\EloquentChallengeLogRepository'
            );
            
            $this->app->bind(
                'App\Libs\Platform\Storage\Entry\EntryRepository',
                'App\Libs\Platform\Storage\Entry\EloquentEntryRepository'
            );
            
            $this->app->bind(
                'App\Libs\Platform\Storage\Question\QuestionRepository',
                'App\Libs\Platform\Storage\Question\EloquentQuestionRepository'
            );
            
            $this->app->bind(
                'App\Libs\Platform\Storage\Metric\MetricRepository',
                'App\Libs\Platform\Storage\Metric\EloquentMetricRepository'
            );
            
            $this->app->bind(
                'App\Libs\Platform\Storage\User\UserRepository',
                'App\Libs\Platform\Storage\User\EloquentUserRepository'
            );
	}

}
