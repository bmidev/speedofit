<?php namespace App\Libs\Platform\Storage\Metric;

interface MetricRepository {
    public function all();

    public function create($input);

    public function delete($id);

    public function find($id);

    public function getMetricValue($is_swimmer, $metric_code);
    
    public function listing($limit, $active, $fields, $filters, $sort, $with);

    public function update($id,$input);

    public function view($id, $active, $fields, $with);
}
