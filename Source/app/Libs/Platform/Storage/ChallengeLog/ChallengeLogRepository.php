<?php namespace App\Libs\Platform\Storage\ChallengeLog;

interface ChallengeLogRepository {
    public function all();
    
    public function create($input);
    
    public function delete($id);

    public function find($id);
        
    public function listing($limit, $active, $fields, $filters, $sort, $with);
    
    public function update($id,$input);
    
    public function view($id, $active, $fields, $with);
}
