<?php namespace App\Libs\Platform\Storage\User;

interface UserRepository {
    public function all();
    
    public function create($input);
    
    public function datatables();
    
    public function delete($id);
    
    public function find($id);
    
    public function getUserFromEmail($user_email);
    
    public function listing($limit, $fields, $filters, $sort, $with);
    
    public function update($id,$input);
    
    public function view($id, $fields, $with);
}
