<?php namespace App\Libs\Platform\Storage\Question;

use App\Models\Question;
use DB;
use Yajra\Datatables\Datatables;

class EloquentQuestionRepository implements QuestionRepository {
    protected $model;

    /**
     * Contructor method
     * 
     * @param Question $model
     */
    public function __construct(Question $model) {
        $this->model = $model;
    }
    
    /**
     * Method to fetch all the entries from the table
     */
    public function all() {
        return $this->model->all();
    }

    /**
     * Method to create a new entry in the table
     * 
     * @param array $data : array containing the new entry's data
     */
    public function create($data) {
        return $this->model->create($data);
    }
    
    /***
     * Data Table
     */
    public function datatables() {
        $result = $this->model->select([
                    'is_active',
                    'id',
                    'question',
                    'is_swimmer'
                ]);
        
        return Datatables::of($result)
                ->make(true);
    }
    
    /**
     * Method to delete an existing entry from the database
     * 
     * @param int $id : id of the entry
     */
    public function delete($id) {
        $resource = $this->find($id);

        return $resource->delete();
    }
    
    /**
     * Method to fetch and return a particular record from the table by 'id'
     * 
     * @param int $id : id of the entry
     */
    public function find($id) {
        return $this->model->find($id);
    }
    
    /**
     * Get a paginated listing
     * 
     * @param int $limit
     * @param boolean $active
     * @param array $fields
     * @param array $filters
     * @param string $sort
     * @param array $with
     * @throws DBException
     */
    public function listing($limit=25, $active=true, $fields=[], $filters=[], $sort=['id'], $with=[]) {
        $order = 'ASC';	// default query sorting order
        $query = $this->model->newQuery();
        $tableName = $this->model->getTable();

        if ($active) {
            $query->where($tableName . '.is_active', '=', 1);
        }
        if (!$fields) {
            $fields = [$tableName . '.*'];
        }
        if ($filters) {
            if (isset($filters['id'])) {
                $query->where($tableName . '.id', '=', $filters['id']);
            }
            if (isset($filters['is_swimmer'])) {
                $query->where($tableName . '.is_swimmer', '=', $filters['is_swimmer']);
            }
            if(isset($filters['allow_id'])){
                $query->whereIn('id', $filters['allow_id']);
            }
            if (isset($filters['search'])) {
                $query->where($tableName . '.name', 'LIKE', '%' . $filters['search'] . '%');
            }
        }
        if ($with) {
            $with = $this->model->processWithSelects($with);
        }

        try {
            $query->with($with);
            foreach ($sort as $val) {
                $query->orderBy($val, $order);
            }

            return $query->paginate($limit, $fields);
        }
        catch (Exception $e) {
            throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
    
    /**
     * Method to get N random Question from existing entry in the database
     * 
     * @param int $n_question
     * @param boolean $is_swimmer
     * @return model_object
     */
    public function getNQuestionIDs($n_question, $is_swimmer) {
        $resource = $this->model
                ->where('is_swimmer', $is_swimmer)
                ->whereNull('deleted_at')
                ->where('is_active', 1)
                ->orderBy(DB::raw('RAND()'))
                ->take($n_question)
                ->select('id')
                ->get()
                ->toArray();
        
        $questionIDs = [];
        foreach($resource as $questionID){
            $questionIDs[] = $questionID['id'];
        }
        
        return $questionIDs;
    }
    
    /**
     * Method to update an existing entry in the database
     * 
     * @param int $id : id of the entry
     * @param array $data : array containing the entry's updated data
     * @return model_object
     */
    public function update($id, $data) {
        $resource = $this->model->find($id);
        $resource->fill($data);
        $resource->save();

        return $resource;
    }
    
    /**
     * Method to fetch an entry along with the respective data based on the criteria
     * 
     * @param int $id
     * @param boolean $active
     * @param string $fields
     * @param array $with
     * @return model_object
     * @throws DBException
     */
    public function view($id, $active=true, $fields=[], $with=[]) {
        $query = $this->model->newQuery();
        $tableName = $this->model->getTable();

        if ($active) {
                $query->where($tableName . '.is_active', '=', 1);
        }
        if (!$fields) {
                $fields = [$tableName . '.*'];
        }
        if ($with) {
                $with = $this->model->processWithSelects($with);
                $query->with($with);
        }

        try {
                return $query->with($with)->where($tableName . '.id', '=', $id)->first($fields);
        }
        catch (Exception $e) {
                throw new DBException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
    
    /**
     * Method to fetch an entry along with the respective data based on the criteria
     * 
     * @param int $id
     * @param boolean $active
     * @param string $fields
     * @return model_object
     * @throws DBException
     */
    public function viewInfo($id) {
        return $this->model->find($id)->answers;
    }
}