<?php namespace App\Libs\Platform\Storage\Question;

interface QuestionRepository {
    public function all();

    public function create($input);

    public function datatables();
    
    public function delete($id);

    public function find($id);

    public function listing($limit, $fields, $filters, $sort, $with);

    public function getNQuestionIDs($n_question, $is_swimmer);
    
    public function update($id,$input);

    public function view($id, $active, $fields, $with);
    
    public function viewInfo($id);
}
