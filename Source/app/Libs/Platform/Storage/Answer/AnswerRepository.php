<?php namespace App\Libs\Platform\Storage\Answer;

interface AnswerRepository {
    public function all();

    public function create($input);

    public function delete($id);

    public function find($id);

    public function listing($limit, $fields, $filters, $sort, $with);

    public function update($id,$input);

    public function view($id, $fields, $with);
}
