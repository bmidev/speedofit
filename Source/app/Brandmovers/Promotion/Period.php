<?php namespace Brandmovers\Promotion;

use Carbon\Carbon;

class Period {

	/**
	 * Get valid periods for a promotion.  
	 * To package this, it should be convereted to database storage with cached access
	 * 
	 * @return array
	 */
	public static function getPeriods() {
		return [
			0 => [
				'start' => '2015-06-01 00:00:01',
				'end'	=> '2015-06-30 23:59:59'
			],
			1 => [
				'start' => '2015-07-01 00:00:00',
				'end'	=> '2015-07-30 23:59:59'
			]
		];
	}
	

	/**
	 * Get periods in a format to be selected from an HTML select
	 * 
	 * [ 
	 * 	0 => '03/15/2015 12:00 PM - 03/25/15 11:59:59'
	 * ]
	 * 
	 * @return array
	 */	
	public static function periodSelectOptions() {
		return array_map(function($period) {
			return date("m/d/y g:i A", strtotime($period['start'])) . " - " . date("m/d/y g:i A", strtotime($period['end']));
		}, self::getPeriods());
	}
	
	
	/**
	 * Determine if a date is within any given promotion period
	 * 
	 * @param string $date - Date coming from database format of 'Y-m-d H:i:s'
	 * @return bool
	 */
	public static function isWithinPeriod($date) {
		return (self::getPeriodContained($date) !== null);
	}


	/**
	 * Get the period a given date falls within
	 * 
	 * @param string $date - Date coming from database format of 'Y-m-d H:i:s'
	 * @return Period | null
	 */
	public static function getPeriodContained($date) {
		
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
		
		foreach( self::getPeriods() as $period ) {
			$start = Carbon::createFromFormat('Y-m-d H:i:s', $period['start']);
			$end = Carbon::createFromFormat('Y-m-d H:i:s', $period['end']);	

			if( $date->gte($start) && $date->lte($end) ) {
				return $period;
			}
		}
		
		return null;
		
	}

}
		