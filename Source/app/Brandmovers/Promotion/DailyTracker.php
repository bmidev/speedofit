<?php namespace Brandmovers\Promotion;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Brandmovers\Promotion\WinnerGenerator;

class DailyTracker extends Model {
	
	const MAX_RETRIES = 3;
	
	protected $dates = ['giveaway_date', 'expires_date'];
	
	public function drawings() {
		return $this->hasMany('Brandmovers\Promotion\WinnerDrawing');
	}
	
	
	
	/**
	 * Process all days that need to be updated
	 */
	public static function processDays() {
		
		$startDate = Carbon::now();
		$startDate->subDay();
		
		$days = self::where('complete', false)
					->where('giveaway_date', '<=', $startDate)
					->get();

		foreach($days as $day) {
			$day->processDay();
		}

	}
	
	
	/**
	 * Process a given daily tracker, making any necessary decisions
	 */
	public function processDay() {
		echo "<pre>";
		echo "Processing " . $this->giveaway_date . "\n";
				
		
		if( $this->attempts_made >= self::MAX_RETRIES ) {
			// We've already completed all necessary attempts, shut it down
			$this->complete = 1;
			$this->save();
			echo "Complete\n\n";
			return;
		}
		
		if( $this->expiration_pending == true ) {
			// We're still waiting on an expiration date to be set for the previous winner drawing.
			// This means a winner has been selected, but they have not been contacted (which sets the 
			// expiration date)
			echo "Expiration Pending\n\n";
			return;
		}
		
		if( $this->expires_date > Carbon::now() ) {
			// We're still waiting for the expiration date to pass to reprocess
			echo "Expiration still valid\n\n";
			return;
		}
		
		
		// Daily Event has not reached maximum retries and has expired; draw new winner
		$periods = Period::getPeriods();
		$period = $periods[1];
		
		// Get Prize
		$prize = Prize::where('name', Prize::PRIZE_SWEETARTS_DAILY)->first();

		// Generate Winner
		echo "Drawing Winner\n";
		
		// Maximum entry date of 23:59:59 of this date
		$maxDate = $this->giveaway_date;
		$maxDate->hour = 23;
		$maxDate->minute = 59;
		$maxDate->second = 59;
		
		// Override the first day to check for the second (TODO: this is a hard-coded temporary fix for not having entries the first day)
		if( $this->id == 1 ) {
			$maxDate->addDay();
		}
		
		$winnerSelected = WinnerGenerator::selectWinner($prize, $period, $this->id, $maxDate);
		
		if( $winnerSelected ) {
			echo "Winner Selected\n\n";
			
			$this->attempts_made++;
			$this->expiration_pending = true;
			
			if( $this->attempts_made >= self::MAX_RETRIES ) {
				$this->complete = true;
			}
			$this->save();
		}
		echo "Completed\n\n\n";
	}
	
	
	public function status() {
		if( $this->complete ) {
			return "Complete";
		}
		
		if( $this->attempts_made > 0 ) {
			return "Pending";
		}
		
		return "Not Started";
	}
	
}
