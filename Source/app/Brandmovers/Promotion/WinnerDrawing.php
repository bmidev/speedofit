<?php namespace Brandmovers\Promotion;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Brandmovers\Promotion\DailyTracker;
use Brandmovers\Social\Post;
use Brandmovers\Social\Twitter\OAuth;
use Twitter;
use Image;


class WinnerDrawing extends Model {

	const EXPIRY_DAYS = 3;
	
	/**
	 * Dates
	 */
	protected $dates = ['giveaway_date', 'expiry_date'];

	
	/**
	 * Social Post Association
	 */
	 public function post() {
	 	return $this->belongsTo('Brandmovers\Social\Post');
	 }
	 
	 
	/**
	 * Prize Association
	 */
	public function prize() {
		return $this->belongsTo('Brandmovers\Promotion\Prize');
	}
	
	/**
	 * Daily Tracker Association
	 */
	public function daily_tracker() {
		return $this->belongsTo('Brandmovers\Promotion\DailyTracker');
	}
	
	
	public function winner_info() {
		return $this->hasOne('Brandmovers\Promotion\WinnerInfo', 'drawing_id');
	}
	
	
	
	/**
	 * Scope: Pending Contacts - List of users that need to be contacted
	 */
	public function scopePendingContacts($query, $network = null) {
		
		if( $network ) {
			$query->join('social_posts', 'winner_drawings.post_id', '=', 'social_posts.id')
				->where('social_posts.network', $network)
				->select(['social_posts.*', 'winner_drawings.*']);  // Ensure Winner Drawing is set second, otherwise Laravel will overwrite the ID field with the post ID
		}
		
		return $query->where('approved', true)->where('contacted', 0);
	}
	
	
	/**
	 * Approve this winner drawing
	 */
	public function approve() {
		$this->approved = true;
		$this->reviewed = true;
		$this->reviewed_at = Carbon::now();
		$this->expiry_date = Carbon::now()->addDays(self::EXPIRY_DAYS); // Set default expiry, this may be overriden later
		
		// Download and process the photo
		$this->image_local = $this->savePhoto();
		
		$this->save();
		return $this;
	}
	
	
	/**
	 * Reject this winner drawing
	 */
	public function reject() {
		$this->approved = false;
		$this->reviewed = true;
		$this->reviewed_at = Carbon::now();
		$this->save();
		return $this;
	}
	
	
	/**
	 * Determine if this winner drawing has expired
	 */
	public function hasExpired() {
		
		// If an expiration date hasn't been set, consider it invalid
		if( $this->expiry_date == null )
			return true;
		
		$today = Carbon::now();
		return $today->gt($this->expiry_date);
	}


	public function status() {
		if( $this->confirmed == true )
			return "Winner Confirmed";
		
		if( $this->expiry_date !== null && $this->hasExpired() )
			return "Expired";
		
		if( $this->contacted == true ) {
			if( $this->contact_result ) {
				return $this->contact_result;
			}
			
			return "Contacted";
		}
		
		return "Pending";
	}
	
	
	/**
	 * Mark this entry as contacted 
	 */
	public function markContacted() {
		$this->contacted = true;
		$this->expiry_date = Carbon::now()->addDays(self::EXPIRY_DAYS);
		$this->save();
		
		if( $this->daily_tracker_id ) {
			$this->daily_tracker->expiration_pending = false;
			$this->daily_tracker->expires_date = $this->expiry_date;
			$this->daily_tracker->save();
		}
		
		return $this;
	}

	
	/**
	 * Generate message to be sent to user for confirmation
	 * 
	 * @return string
	 */	
	public function getContactMessage() {
		$msg = $this->prize->prize_message_short . "  Claim your prize at " . 
				$this->confirmationUrl() . " in 72 hours or forfeit ur prize";
				
				
		if( $this->post->network == Post::NETWORK_INSTAGRAM ) {
			$msg .= ". Your URL is unique and case-sensitive.";
		}
		
		return $msg;
	}
	

	/**
	 * Get link for user to confirm their information and entry
	 * 
	 * @return string
	 */
	public function confirmationUrl($disableBitly = false) {
		if( !empty($this->bitly_url) ) {
			return $this->bitly_url;		
		}
		
		
		$url = url('/user-confirmation?w='.$this->id.'&h='.$this->hash);
		
		// Bitly fails and considers "http://localhost" an invalid URL 
		$url = str_replace("localhost", "www.localhost.com", $url);
		
		try {
			$client = new Client();
			$client->setDefaultOption('verify', false);
			$response = $client->get('https://api-ssl.bitly.com/v3/shorten', [
				'query' => [
					'access_token' => '459b0f8cae406081221250f2c9ea190428b60509',
					'longUrl' => $url
				]
			]);
			$json = $response->json();
			$url = $json['data']['url'];
			
			$this->bitly_url = $url;
			$this->save();
		}
		catch(\Exception $e) {
			throw $e;
			return $url;
		}

		return $url;
	}
	
	
	
	/**
	 * Send message to the winner (this method is available for Twitter ONLY)
	 */
	public function sendTwitterContact() {
		
		if( $this->post->network != Post::NETWORK_TWITTER ) {
			throw new Exception("Cannot send confirmation to non-Twitter post");
		}
			
		if (app()->environment() !== "production") {
			die('Disabled sending of messages not in production');			
		}

		// Get Twitter OAuth Info
		$oauth = OAuth::getLatest();
		Twitter::setToken($oauth->oauth_token, $oauth->oauth_secret);
		
		
		$params = [
			'screen_name'	=> $this->post->author_username,
			//'screen_name'	=> 'ryandevtest',
			'text'			=> $this->getContactMessage()
		];
	
		$response = (object)Twitter::postDirectMessagesNew($params);
		$notFollowing = false;
		$errors = false;
		
		if( $response->httpstatus == 403 ) {
			if( isset($response->errors) && is_array($response->errors) ) {
				$errors = true;
				
				foreach($response->errors as $error) {
					if( $error->code == 150) {
						$notFollowing = true;
					}
				}
			}
		}
		
		if( $notFollowing === true ) {
			$this->contact_result = "Not Following";
		}
		elseif( $errors === true )  {
			$this->contact_result = "Error";
		}
		else {
			$this->contact_result = "Sent";
		}
		
		$this->contact_result_response = json_encode($response);

		// This will save to update the information set above
		$this->markContacted();

	}
	

	/**
	 * Mark a winner drawing as 'confirmed', updating the daily drawing if necessary
	 * 
	 * @return void
	 */
	public function confirmWinner($winnerInfoId) {
		$this->winner_info_id = $winnerInfoId;
		$this->confirmed = 1;
		$this->save();
		
		if( $this->daily_tracker_id != null ) {
			// Update the daily tracker
			$tracker = DailyTracker::find($this->daily_tracker_id);
			$tracker->complete = 1;
			$tracker->save();
		}
	}
	
	
	/**
	 * Save an entry photo to local storage
	 * 
	 * @return string filename of saved file
	 */	
	public function savePhoto() {
		$filename = $this->id ."-". uniqid() . ".jpg";  // Note that Instagram and Twitter always save images as JPG
		$storagePath = self::getImageStoragePath() . $filename;

		$img = Image::make($this->post->image);
		$img->fit(500);
		$img->save($storagePath, 100);
		
		return $filename;
	}
	
	
	/**
	 * Get the public path where user entry photos are stored
	 * 
	 * @return string
	 */
	public static function getImageStoragePath() {
		return public_path() . '/img/entries/';
	}
	
	
	/**
	 * Return the full URL for the local image storage
	 * 
	 * @return string
	 */
	public function imageLocalUrl() {
		return url('/img/entries/' . $this->image_local);
	}
	
}
