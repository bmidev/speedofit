<?php namespace Brandmovers\Promotion;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Brandmovers\Social\Post;
use Brandmovers\Promotion\Period;

class WinnerInfo extends Model {

	protected $table = "winner_info";
	
	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'address_1',
		'address_2',
		'city',
		'state',
		'zip', 
		'phone'
	];


	public function winner_drawing() {
	 	return $this->belongsTo('Brandmovers\Social\WinnerDrawing');
	}

}