<?php namespace App\Brandmovers\Charts;

use Illuminate\Support\Facades\View;

/**
 * This class generates a line chart.
 *
 * @exception
 * @author Brad Estey
 * @package BMI Component
 */

class LineChart {

	private $data = array();
	private $options;
	private $start_date;
	private $end_date;

	public function __construct($data = FALSE, $options = array(), $start_date = NULL, $end_date = NULL)
	{
		// Set default options.
		$this->options['chartId'] = 'linechart';
		$this->options['labelName'] = 'Day';
		$this->options['pointName'] = 'Visitors';
		$this->options['width'] = 614;
		$this->options['height'] = 160;
		$this->options['legend'] = 'none';
		$this->options['areaLeft'] = 50;
		$this->options['areaTop'] = 20;
		$this->options['areaWidth'] = '90%';
		$this->options['areaHeight'] = '65%';

		// Set options.
		if (is_array($options)) {
			foreach($options as $key => $value)
			{
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}

		// Set data.
		if ($data) { $this->data = $data; }

		$this->start_date = $start_date;
		$this->end_date = $end_date;
	}

	// ==============================
	// ! Accessors and Manipulators
	// ==============================

	// Get Raw Chart Data
	public function getData() {
	    if (is_array($this->data)) { return $this->data; }
	    else { return FALSE; }
	}

	// formatted data for the series attribute for the line chart
	public function getSeriesData()
	{
	    if (empty($this->data))
	    {
	    	return '[]';
	    }

		$series_data = array();
		foreach($this->data as $row)
		{
			$series_data[] = $row['point'];
		}

		return json_encode($series_data);
	}

	// Set Chart Data
	public function setData() {
		if ($data) { $this->data = $data; }
	}


	// Get Chart ID -- clean: convert to alphanumeric, lowercase and replace spaces with underscores.
	public function getChartId($clean = FALSE) {
	    if (!$clean) { return $this->options['chartId']; }
	    else { return $this->options['chartId']; }
	}


	// Get Chart Display Options
	public function getOptions() {
	    if (is_array($this->options)) { return $this->options; }
	    else { return FALSE; }
	}

	// Set Chart Display Options
	public function setOptions($options = FALSE) {
		if (is_array($options)) {
			foreach($options as $key => $value)
			{
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}
	}

	// Get Chart Data by Key.
	public function __call($name, $parameters) {
		$name = preg_replace('/^get/','',$name);

		// lcfirst() doesn't work on all our servers.. :(
		$name = strtolower(substr($name, 0, 1)) . substr($name, 1);

		if (array_key_exists($name, $this->options)) {
			return $this->options[$name];
		}

		return FALSE;
	}

	public function getDate() {
		return array('start_date' => $this->start_date, 'end_date' => $this->end_date);
	}

	public function startDay() {
		$date = $this->getDate();
		return date('j', $date['start_date']);
	}

	public function startMonth() {
		$date = $this->getDate();
		/*
			subtract one, for highcharts timeline chart, for some
			reason it starts displaying data for a month ahead of the
			start date (pointStart)
		*/
		return date('n', $date['start_date']) - 1;
	}

	public function startYear() {
		$date = $this->getDate();
		return date('Y', $date['start_date']);
	}

	// Count Total Rows in Data Array.
	public function getCount() {
	    if (is_array($this->data)) { return count($this->data); }
	    else { return FALSE; }
	}

	// Generate HTML to show chart.
	public function generateChart() {
		return View::make('admin.charts.line_chart', array('data' => $this));
	}

	public function getColors($as_string = FALSE) {
		if ($as_string) {
			$str = '';
			$colors = explode(',', $this->options['colors']);
			foreach ($colors as $color) {
				$str.= ',\'#' . $color . '\'';
			}
			return substr($str, 1);
		}
		return $this->options['colors'];
	}
}