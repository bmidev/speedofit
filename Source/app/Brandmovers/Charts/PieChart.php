<?php namespace App\Brandmovers\Charts;

use Illuminate\Support\Facades\View;

/**
 * This class generates a pie chart.
 *
 * @exception
 * @author Brad Estey
 * @package BMI Component
 */

class PieChart {

	private $data = array();
	private $options;

	public function __construct($data = FALSE, $options = array()) {

		// Set default options.
		$this->options['width'] = 280;
		$this->options['height'] = 130;
		$this->options['colors'] = '5b5c77,f9f476,4ac260,f48b36,1c82d8';
		$this->options['interactive'] = FALSE;

		$this->colors = array('#5B5C77', '#D9D676','#90D669', '#8EAC4F', '#C98956', '#1C82D8');

		// These variables only required for Interactive Pie Charts.
		$this->options['chartId'] = 'piechart';
		$this->options['areaLeft'] = 20;
		$this->options['areaTop'] = 20;
		$this->options['areaWidth'] = '100%';
		$this->options['areaHeight'] = '90%';

		// Set options.
		if (is_array($options)) {
			foreach($options as $key => $value) {
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}

		// Set data.
		if ($data) { $this->data = $data; }
	}

	// ==============================
	// ! Accessors and Manipulators
	// ==============================

	// Get Raw Chart Data
	public function getData() {
	    if (is_array($this->data)) {
	    	$data = array();
	    	$i = 0;

	    	foreach($this->data as $key => $value) {
	    		$data[$i]['label'] = $key;
	    		$data[$i]['point'] = $value;
	    		$i++;
	    	}
	    	return $data;
	    }

	    return FALSE;
	}

	// formatted data for highcharts pie chart
	public function getSeriesData() {
	    if (is_array($this->data)) {
	    	$data = array();
	    	$i = 0;

	    	$colors = $this->colors;
			$series_data = '';

	    	foreach($this->data as $key => $value) {
                if( $i < sizeof($colors)) {
                    $series_data .= "{";
                    $series_data .= "
					name: '" . $key . "',
					y: " . $value . ",
					color: '" . $colors[$i] . "',
				";
                    $series_data .= "},";

                    $i++;
                } else {
                    $series_data .= "{";
                    $series_data .= "
					name: '" . $key . "',
					y: " . $value . ",
					color: '" . array_rand($colors) . "',
				";
                    $series_data .= "},";
                }
	    	}
	    	return $series_data;
	    }

	    return FALSE;
	}

	// Set Chart Data
	public function setData() {
		if ($data) { $this->data = $data; }
	}

	// Humanize numbers with commas and such.
	public function humanizedData() {
		$data = array();
		foreach($this->data as $key => $value) {
			if (is_numeric($value) && $value == intval($value)) {
				$data[$key] = number_format($value, 0, '.', ',');
			}
		}
		$data = $data + $this->data;
		return $data;
	}

	// Get Chart Display Options
	public function getOptions() {
	    if (is_array($this->options)) { return $this->options; }
	    else { return FALSE; }
	}

	// Set Chart Display Options
	public function setOptions($options = FALSE) {
		if (is_array($options)) {
			foreach($options as $key => $value) {
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}
	}

	// Get Chart Data by Key.
	public function __call($name, $parameters) {
		$name = preg_replace('/^get/','',$name);

		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		// lcfirst() doesn't work on all our servers.. :(
		$name = strtolower(substr($name, 0, 1)) . substr($name, 1);

		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		if (array_key_exists($name, $this->options)) {
			return $this->options[$name];
		}

		return FALSE;
	}

	// Count Total Rows in Data Array.
	public function getCount() {
	    if (is_array($this->data)) { return count($this->data); }
	    else { return FALSE; }
	}


	// Calculate Total of Values in Data Array.
	public function total() {
	    $total = 0;
	    foreach($this->data as $key => $value) {
	    	$total = $total + $value;
	    }
	    return $total;
	}

	// Generate HTML to show chart.
	public function generateChart() {
		return View::make('admin.charts.pie_chart', array('data' => $this))->render();
	}

	// Encode Chart Labels into a pipe delimited string
	public function encodeChartLabels() {
		$data = '';
		foreach($this->data as $key => $value) {
			$data .= '|' . $key;
		}
		return substr($data, 1);
	}

	// Encode Chart Values into a comma delimited string
	public function encodeChartValues() {
		$data = '';
		foreach($this->data as $key => $value) {
			$data .= ',' . $value;
		}
		return substr($data, 1);
	}

	public function isInteractive() {
		return $this->options['interactive'];
	}

	public function getColors($as_string = FALSE) {
		$data = array();

		if ($as_string) {
			$str = '';
			$i = 0;
			$colors = explode(',', $this->options['colors']);
			foreach ($colors as $color) {
				$str.= ',\'#' . $color . '\'';
				$data[$i] = $color;
				$i++;
			}
			return substr($str, 1);
		}
		//return $this->options['colors'];
		return $data;
	}

	// Get Chart ID -- clean: convert to alphanumeric, lowercase and replace spaces with underscores.
	public function getChartId($clean = FALSE) {
	    if (!$clean) { return $this->options['chartId']; }
	    else { return $this->options['chartId']; }
	}
}