<?php namespace App\Brandmovers\Charts;

use Illuminate\Support\Facades\View;

/**
 * This class generates a pie chart.
 *
 * @exception
 * @author Brad Estey
 * @package BMI Component
 */

class Table {

	private $data = array();
	private $options;

	public function __construct($data = FALSE, $options = FALSE) {

		// Set default options.
		$this->options['chartId'] = 'my_table';
		$this->options['page'] = 1;
		$this->options['perPage'] = 10;
		$this->options['totalPages'] = FALSE;
		$this->options['width'] = '100%';
		$this->options['controller'] = 'charts';

		// Set options.
		if (is_array($options)) {
			foreach($options as $key => $value) {
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}

		// Set data.
		if ($data) { $this->data = $data; }
	}

	// ==============================
	// ! Accessors and Manipulators
	// ==============================

	// Get Raw Chart Data
	public function getData() {
	    if (is_array($this->data)) { return $this->data; }
	    else { return FALSE; }
	}

	// Set Chart Data
	public function setData() {
		if ($data) { $this->data = $data; }
	}

	// Get Chart Display Options
	public function getOptions() {
	    if (is_array($this->options)) { return $this->options; }
	    else { return FALSE; }
	}

	// Set Chart Display Options
	public function setOptions($options = FALSE) {
		if (is_array($options)) {
			foreach($options as $key => $value) {
				if (array_key_exists($key, $this->options)) { $this->options[$key] = $value; }
			}
		}
	}

	// Get Chart Data by Key.
	public function __call($name, $parameters) {
		$name = preg_replace('/^get/','',$name);

		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		// lcfirst() doesn't work on all our servers.. :(
		$name = strtolower(substr($name, 0, 1)) . substr($name, 1);

		if (array_key_exists($name, $this->data)) {
			return $this->data[$name];
		}

		if (array_key_exists($name, $this->options)) {
			return $this->options[$name];
		}

		return FALSE;
	}

	// Count Total Rows in Data Array.
	public function getCount() {
	    if (is_array($this->data)) { return count($this->data); }
	    else { return FALSE; }
	}

	// Generate HTML to show chart.
	public function generateChart() {
		return View::make('dashboard::charts.table', array('data' => $this));
	}

	// Get Chart ID -- clean: convert to alphanumeric, lowercase and replace spaces with underscores.
	public function getChartId($clean = FALSE) {
	    if (!$clean) { return $this->options['chartId']; }
	    else { return $this->options['chartId']; }
	}

	public function getWidth() {
		if (is_numeric($this->options['width']) && $this->options['width'] == intval($this->options['width'])) {
			return $this->options['width'] . 'px';
		} else {
			return $this->options['width'];
		}
	}

	// Generate URL prefix for paging.
	public function getPrefix($form_data) {
	    $prefix = base_url() . strtolower($this->options['controller']) . '/per_page/' . $this->options['perPage'];
	    foreach ($form_data as $key => $value) {
	    	$prefix .= '/' . $key . '/' . $value;
	    }
	    $prefix .= '/page/';
	    return $prefix;
	}

}