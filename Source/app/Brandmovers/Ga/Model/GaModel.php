<?php

namespace App\Brandmovers\Ga\Model;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\Brandmovers\Ga\Gapi;

class GaModel {

	protected $ga_profile_id;

	public function __construct($ga_profile_id) {
		$this->ga_profile_id = $ga_profile_id;

//        $args = ['client_email' => Config::get('admin.ga_client_email'), 'key' => Config::get('admin.ga_key_file')];
		$args = ['client_email' => Config::get('admin.ga_email'), 'password' => Config::get('admin.ga_password')];
		$this->gapi = new Gapi($args);
	}

	// =======================================
	//  Get Basic Stats. Cache Returned Data.
	// =======================================

	public function basicStats($start_date, $end_date) {
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'basicStats-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('newVisits', 'visits', 'visitors', 'pageviews', 'uniquePageviews', 'avgTimeOnSite', 'bounces');
			$dimensions = array('year', 'month', 'day');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, null, null, $start_date, $end_date);
			$data = $this->gapi->getMetrics();

			// Create custom calculations.

			$custom['avgTimeOnSite'] = $this->sec2hms(round($data['avgTimeOnSite']));
			if ($data['visits'] != 0) {
				$custom['pagesPerVisit'] = round(($data['pageviews'] / $data['visits']), 2);
				$custom['bounceRate'] = round((($data['bounces'] / $data['visits']) * 100), 2) . '%';
				$custom['newVisitRate'] = round((($data['newVisits'] / $data['visits']) * 100), 2) . '%';
			} else {
				$custom['pagesPerVisit'] = '0';
				$custom['bounceRate'] = '0%';
				$custom['newVisitRate'] = '0%';
			}
			$data = array_merge($data, $custom);

			if ($this->cachingOn()) {
				Log::info('Cache miss: basicStats');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: basicStats');
			$data = Cache::get($cache_file);
		}
		return $data;
	}

	// ================================================
	//  Get Daily Total Visitors. Cache Returned Data.
	// ================================================

	public function dailyVisits($start_date, $end_date) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'dailyVisits-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$dimensions = array('year', 'month', 'day');
			$metrics = array('visits');
			$sort = array('year', 'month', 'day');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date, 1, 300);

			foreach ($this->gapi->getResults() as $result) {
				$data[] = array
					(
					'label' => date('M j', strtotime($result->getYear() . '-' . $result->getMonth() . '-' . $result->getDay())),
					'point' => $result->getVisits(),
				);
			}

			if ($this->cachingOn()) {
				Log::info('Cache miss: dailyVisits');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: dailyVisits');
			$data = Cache::get($cache_file);
		}
		return $data;
	}

	// =================================================
	//  Get Total Mobile Visitors. Cache Returned Data.
	// =================================================

	public function mobileVisitors($start_date, $end_date, $limit = 5) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'mobileVisitors-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file)) {
			$metrics = array('visits');
			$dimensions = array('isMobile', 'operatingSystem');
			$filter = 'isMobile == Yes';
			$sort = array('-visits');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, $filter, $start_date, $end_date);

			if ($limit) {
				$i = 1;
			}
			foreach ($this->gapi->getResults() as $result) {
				if ($limit) {
					if ($i <= $limit) {
						$data[$result->getOperatingSystem()] = $result->getVisits();
						$i++;
					} else {
						if (!isset($data['Other'])) {
							$data['Other'] = 0;
						}
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} else {
					$data[$result->getOperatingSystem()] = $result->getVisits();
				}
			}
			if ($this->cachingOn()) {
				Log::info('Cache miss: mobileVisitors');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: mobileVisitors');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	// =====================================================
	//  Get Total Visitors by Browser. Cache Returned Data.
	// =====================================================

	public function getVisitorsByBrowsers($start_date, $end_date, $limit = 5) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByBrowsers-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('visits');
			$dimensions = array('browser');
			$sort = array('-visits');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date);

			if ($limit) {
				$i = 1;
			}
			foreach ($this->gapi->getResults() as $result) {
				if ($limit) {
					if ($i <= $limit) {
						$browser = trim($result->getBrowser());
						if ($browser == 'Mozilla Compatible Agent') {
							$browser = 'Mozilla';
						}
						$data[$browser] = $result->getVisits();
						$i++;
					} else {
						if (!isset($data['Other'])) {
							$data['Other'] = 0;
						}
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} else {
					$data[$result->getBrowser()] = $result->getVisits();
				}
			}
			if ($this->cachingOn()) {
				Log::info('Cache miss: getVisitorsByBrowsers');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getVisitorsByBrowsers');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	// ====================================================
	//  Get Total Visitors by Medium. Cache Returned Data.
	// ====================================================

	public function getVisitorsByMedium($start_date, $end_date, $limit = 5) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByMedium-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('visits');
			$dimensions = array('medium');
			$sort = array('-visits');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date);

			if ($limit) {
				$i = 1;
			}
			foreach ($this->gapi->getResults() as $result) {
				if ($result->getMedium() == '(none)') {
					$key = 'Direct';
				} else {
					$key = ucfirst($result->getMedium());
				}
				if ($limit) {
					if ($i <= $limit) {
						$data[$key] = $result->getVisits();
						$i++;
					} else {
						if (!isset($data['Other'])) {
							$data['Other'] = 0;
						}
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} else {
					$data[$key] = $result->getVisits();
				}
			}
			if ($this->cachingOn()) {
				Log::info('Cache miss: getVisitorsByMedium');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getVisitorsByMedium');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	// ======================================================================
	//  Get Total Visitors from United States by State. Cache Returned Data.
	// ======================================================================

	public function getVisitorsFromUnitedStates($start_date, $end_date, $limit = 50) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsFromUnitedStates-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('visits');
			$dimensions = array('region');
			$sort = array('-visits');
			$filter = 'country == United States';

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, $filter, $start_date, $end_date, 1, $limit);

			foreach ($this->gapi->getResults() as $result) {
				$data[] = array('state' => $result->getRegion(),
					'visits' => $result->getVisits());
			}

			if ($this->cachingOn()) {
				Log::info('Cache miss: getVisitorsFromUnitedStates');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getVisitorsFromUnitedStates');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	public function getVisitorsFromCountry($country, $start_date, $end_date, $limit = 50) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsFromCountry-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('visits');
			$dimensions = array('region');
			$sort = array('-visits');
			$filter = 'country == ' . $country;
			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, $filter, $start_date, $end_date, 1, $limit);

			foreach ($this->gapi->getResults() as $result) {
				$data[] = array('state' => $result->getRegion(),
					'visits' => $result->getVisits());
			}

			if ($this->cachingOn()) {
				Log::info('Cache miss: getVisitorsFromCountry');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getVisitorsFromCountry');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	// ======================================================================
	//  Get Total Visitors from United States by State. Cache Returned Data.
	// ======================================================================

	public function getVisitorsByCountry($start_date, $end_date, $limit = 50) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByCountry-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('visits');
			$dimensions = array('country');
			$sort = array('-visits');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date, 1, $limit);

			foreach ($this->gapi->getResults() as $result) {
				$data[] = array('country' => $result->getCountry(),
					'visits' => $result->getVisits());
			}

			if ($this->cachingOn()) {
				Log::info('Cache miss: getVisitorsByCountry');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getVisitorsByCountry');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	// ==========================================
	//  Get Tracked Events. Cache Returned Data.
	// ==========================================

	public function getEvents($start_date, $end_date) {
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getEvents-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$dimensions = array('eventCategory', 'eventAction', 'eventLabel');
			$metrics = array('totalEvents');
			$sort = array('-totalEvents', 'eventCategory', 'eventAction', 'eventLabel');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date, 1, 300);
			foreach ($this->gapi->getResults() as $result) {
				$data[$result->getEventCategory()][$result->getEventAction()][$result->getEventLabel()] = $result->getTotalEvents();
			}

			if ($this->cachingOn()) {
				Log::info('Cache miss: getEvents');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getEvents');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

	public function cachingOn() {
		return Config::get('admin.caching');
	}

	// ============================================
	//  Convert seconds to Hours, Minutes, Seconds
	// ============================================

	public function sec2hms($sec, $pad_hours = false) {
		$hms = '';
		$hours = intval(intval($sec) / 3600);
		$hms .= ($pad_hours) ? str_pad($hours, 2, "00", STR_PAD_LEFT) . ':' : $hours . ':';
		$minutes = intval(($sec / 60) % 60);
		$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT) . ':';
		$seconds = intval($sec % 60);
		$hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
		return $hms;
	}

	public function getVisitorsByChannel($start_date, $end_date, $limit = 5){
		$data = array();
		$start_date = date('Y-m-d', $start_date);
		$end_date = date('Y-m-d', $end_date);
		$cache_file = 'getVisitorsByChannel-' . $start_date . '-' . $end_date;

		if (!Cache::has($cache_file) || !$this->cachingOn()) {
			$metrics = array('visits');
			$dimensions = array('channelGrouping');
			$sort = array('-visits');

			$this->gapi->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort, null, $start_date, $end_date);

			if ($limit) {
				$i = 1;
			}
			foreach ($this->gapi->getResults() as $result) {

				$key = ucfirst($result->getChannelGrouping());
				if ($limit) {
					if ($i <= $limit) {
						$data[$key] = $result->getVisits();
						$i++;
					} else {
						if (!isset($data['Other'])) {
							$data['Other'] = 0;
						}
						$data['Other'] = $data['Other'] + $result->getVisits();
					}
				} else {
					$data[$key] = $result->getVisits();
				}
			}

			if ($this->cachingOn()) {
				Log::info('Cache miss: getVisitorsByChannel');
				$expiresAt = Carbon::now()->addMinutes(30);
				Cache::put($cache_file, $data, $expiresAt);
			}
		} else {
			Log::info('Cache hit: getVisitorsByChannel');
			$data = Cache::get($cache_file);
		}

		return $data;
	}

}

/* End of file ga_model.php */
/* Location: ./application/models/ga_model.php */