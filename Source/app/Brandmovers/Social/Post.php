<?php

namespace Brandmovers\Social;

use App\User;
//use App\Media;
//use App\Challenge;
use Illuminate\Database\Eloquent\Model;
use Brandmovers\Promotion\Period;
use Carbon\Carbon;

class Post extends Model {

	protected $table = "social_posts";

	const NETWORK_TWITTER = "Twitter";
	const NETWORK_INSTAGRAM = "Instagram";

	/**
	 * Mass assignment white-list
	 * 
	 * @var array
	 */
	protected $fillable = [
		'type', 'url', 'image', 'image_thumb', 'video_url', 'title', 'comments', 'author_name', 'author_image', 'author_username', 'author_profile',
		'network', 'network_id', 'network_id_str', 'post_date'
	];

	/**
	 * Save a post, ignoring if it has a duplicate entry constraint violation.  
	 * 
	 * @return OpenTable\Social\Post
	 */
	public function saveIgnoreDuplicates() {

		try {
			return $this->save();
		} catch (\Exception $e) {
			// Catch duplicate entry constraint and ignore
			if ($e->errorInfo[0] == "23000") {
				// Continue
				return false;
			} else {
				throw $e;
			}
		}
	}

	/**
	 * Save a post as media
	 *
	 * @return OpenTable\Social\Post
	 */
	public function savePostAsMedia(User $user, $challenge_id) {
		$current_date_time = date('Y-m-d H:i:s');
		$media = new Media();
		$media->challenge_id = $challenge_id;
		$media->user_id = $user->id;
		$media->network_id = $this->network_id;
		$media->approved = 0;
		$media->source = strtolower($this->getNetwork());
		$media->url_thumb = $this->image_thumb;
		$media->first_name = $user->first_name;
		$media->last_name = $user->last_name;
		$media->zip = $user->zip;
		$media->email = $user->email;
		$media->phone = $user->phone;
		$media->dob = $user->dob;
		$media->story = $this->comments;
		$media->created_at = $current_date_time;
		$media->updated_at = $current_date_time;
		$media->url_original = $this->url;
                $media->ip = '';

		if (!empty($this->video_url)) {
			$media->url_main = $this->video_url;
			$media->type = Media::TYPE_VIDEO;
		} else if ($this->network == 'Twitter' && substr_count($this->image, 'ext_tw_video_thumb') > 0) {
			//http://pbs.twimg.com/ext_tw_video_thumb/625675043901583361/pu/img/xgqKOwcYTto-jOoc.jpg:large
			//twitter videos have this format
			$media->url_main = $this->image;
			$media->type = Media::TYPE_VIDEO;
		} else {
			$media->url_main = $this->image;
			$media->type = Media::TYPE_PHOTO;
		}

		//$media->email_optin = $request->input('email-optin');
		//$media->ip = $_SERVER['REMOTE_ADDR'];
		return $media->save();
	}

	/**
	 * Scope: Unprocessed Entries
	 */
	public function scopeUnprocessed($query) {
		return $query->where('processed', '=', 0);
	}

	public function scopeWithinDates($query, $startDate, $endDate) {
		return $query->whereBetween('post_date', [$startDate, $endDate]);
	}

	/**
	 * Process posts into entries by the specified batch size per run
	 * 
	 * @param int $valid - Reference parameter will update with number of valid entries
	 * @param int $invalid = Reference parameter will update with number of invalid entries
	 * @return int
	 */
	public static function processEntries($batchSize = 1000, &$valid = NULL, &$invalid = NULL) {

		$batch = self::where('processed', '=', 0)->take($batchSize)->get();
		$valid = 0;
		$invalid = 0;

		foreach ($batch as $p) {

			// Manually set network since the feed does not for some reason
			$p->network = $p->getNetwork();

			if ($p->validate()) {
				$p->valid_entry = true;
				$valid++;
			} else {
				$p->valid_entry = false;
				$invalid++;
			}

			$p->processed = true;
			$p->save();
		}

		return count($batch);
	}

	/**
	 * Process the social post and validate them against sweepstakes rules.
	 * Check if the entry was valid, if not copy any entry validation errors
	 * to the post for later reference (if needed)
	 *
	 * @return bool
	 */
	public function validate() {
		$this->valid_entry = 0;
		$this->processed = 0;

		// Make sure an image is specified
		if ($this->image == "") {
			$this->errors = "No image specified.";
			$this->processed = 1;
			return false;
		}

		// Make sure the date is within a given promotion period range
		/*if (!Period::isWithinPeriod($this->post_date)) {
			$this->errors = "Not within a promotion period.";
			$this->processed = 1;
			return false;
		}*/

		/* // Validate one entry per method/day/user
		  if( $this->validateOnePerPeriod() ) {
		  $this->errors = "One entry per period";
		  $this->processed = 1;
		  return false;
		  } */

        $pattern = '/YouGotThisDay[0-9]+/i';
		//$pattern = '/#Day[0-9]+Entry/i';
		//$pattern = '/#AxonDay[0-9]+/i';
        //YouGotThisDay1
		if (preg_match($pattern, $this->comments, $matches)) {
            $day = (int) str_ireplace('YouGotThisDay', '', $matches[0]);
			//$day = (int) str_ireplace(array('#Day', 'Entry'), '', $matches[0]);
			//$day = (int) str_ireplace('#AxonDay', '', $matches[0]);
		} else {
			$this->errors = 'Day not specified';
			$this->processed = 1;
			return false;
		}
		$challenge = Challenge::where('day', $day)->first();
		if (!isset($challenge->id)) {
			$this->errors = 'Invalid Day, Challenge does not exists for day ' . $day;
			$this->processed = 1;
			return false;
		}
		if ($this->getNetwork() === self::NETWORK_TWITTER) {
			$user = User::whereTwitterHandle($this->author_username)->first();
		} else {
			$user = User::whereInstagramHandle($this->author_username)->first();
		}

		if (!isset($user->id)) {
			$this->errors = 'Username handle is not linked with any user';
			return false;
		}

		if (!Media::allowed_for_challenge($user->id, $challenge->id)) {
			$this->errors = 'Media entry already exists for day ' . $day;
			$this->processed = 1;
			return false;
		}

		// Passed all validation so it's a valid entry
		if ($this->savePostAsMedia($user, $challenge->id)) {
			$this->processed = 1;
		}

		$this->errors = '';
		$this->valid_entry = 1;
		return true;
	}

	/**
	 * Validate one entry per method/day/user
	 * 
	 * @return bool
	 */
	public function validateOnePerDay() {
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->post_date);

		return self::where('post_date', '>', $date->toDateString())
						->where('post_date', '<', $date->addDay()->toDateString())
						->where('network', $this->network)
						->where('author_username', $this->author_username)
						->exists();
	}

	/**
	 * Validate one entry per period
	 * 
	 * @return bool
	 */
	public function validateOnePerPeriod() {
		$period = Period::getPeriodContained($this->post_date);

		return self::where('post_date', '>=', $period['start'])
						->where('post_date', '<=', $period['end'])
						->where('network', $this->network)
						->where('author_username', $this->author_username)
						->exists();
	}

	/**
	 * Search the content of a post and extract all #hashtags and @mentions
	 * 
	 * @param array $validTags
	 * @return string
	 */
	public function extractTagsAndMentions() {
		$matches = [];
		preg_match_all("/[#|@]+([a-zA-Z0-9_]+)/", $this->comments, $matches);
		return $matches[0];
	}

	/**
	 * Get the network the post was provided from
	 * 
	 * CSV data from TintUp does not include this for some reason, the
	 * network field is always blank.  Best solution is to figure 
	 * it out from the author's profile URL.
	 * 
	 * @return string
	 */
	public function getNetwork() {

		if (strpos($this->author_profile, 'twitter.com') !== FALSE) {
			return self::NETWORK_TWITTER;
		} elseif (strpos($this->author_profile, 'instagram.com') !== FALSE) {
			return self::NETWORK_INSTAGRAM;
		}

		return null;
	}

	/**
	 * Network Summary
	 * Get breakdown of posts by network
	 * 
	 * Example Return Value:
	 * [
	 * 	 [
	 *     'method'  => 'Twitter'
	 *     'total'   => 89,458
	 *   ]
	 *   ...
	 * ]
	 * 
	 * 
	 * TODO:  This needs ot happen after its processed and add this to the database... (Since TintUp doesn't fill it out)
	 * 
	 * @return array
	 */
	public static function networkSummary() {
		return self::selectRaw('network, COUNT(id) AS total')
						->where('valid_entry', 1)
						->groupBy('network')
						->get()
						->toArray();
	}

	public function toPublicJSON($options = 0) {
		return array(
			'network' => $this->network,
			'network_id' => $this->network_id,
			'url' => $this->url,
			'image' => $this->image,
			'comments' => $this->comments,
			'author' => array(
				'name' => $this->author_name,
				'avatar' => $this->author_image,
				'username' => $this->author_username,
				'profile_url' => $this->author_profile
			),
			'date' => $this->post_date
		);
	}

}
