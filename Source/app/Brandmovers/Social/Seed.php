<?php namespace Brandmovers\Social;

use Brandmovers\Social\Post;
use Faker;

class Seed {

	/**
	 * Generate fake entries using fake data for testing purposes.
	 * 
	 * @param int $count
	 * @return void
	 */
	public static function generateFake($count = 1000) {
		
		$faker = Faker\Factory::create();

		$methods = ["Twitter", "Instagram"];
		
		for($i = 0; $i < $count; $i++) {

			$int = mt_rand(1427447506,1430989906);
			$date = date('Y-m-d H:i:s', $int); 
			
			$content = $faker->sentence(rand(5,15));
			
			$method = $methods[mt_rand(0,1)];
			$username = $faker->username;
			$profile = "http://" . strtolower($method) . ".com/" . $username;
			
			// Pretend ~5% won't have an image specified
			$image = (mt_rand(0,100) < 5) ? '' : $faker->imageUrl."?".mt_rand(0,99999999);
			
			$data = [
				'url' 				=> $faker->url,
				'image'				=> $image,
				'title'				=> '',
				'comments'			=> $content,
				'author_name'		=> $faker->name,
				'author_image'		=> $faker->imageUrl."?".mt_rand(0,99999999),
				'author_username'	=> $username,
				'author_profile'	=> $profile,
				'network'			=> '',
				'visibility'		=> 'APPROVED',
				'post_date'			=> $date,
			];
			
			$post = new Post($data);
			$post->saveIgnoreDuplicates();

		}

	}
	
}
