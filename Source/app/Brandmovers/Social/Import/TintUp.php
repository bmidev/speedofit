<?php namespace Brandmovers\Social\Import;

use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;

use Brandmovers\Social;

class TintUp {
	
	/**
	 * Define order of records for import from CSV
	 * // TODO
	 */
	private $fieldMap = [];

	
	/**
	 * Import CSV line by line creating Social\Post objects, allowing for 
	 * individual record processing.  (Slower process, not recommended for
	 * very large CSV files)
	 * 
	 * CSV import based on TintUp CSV format.
	 * 
	 * Returns total records imported.
	 * 
	 * @param string $file
	 * @return int
	 */
	public static function importSingles($file) {
		
		$recordsChecked = 0;
		$recordsValid = 0;
		
		$lexer = new Lexer(new LexerConfig());
		$interpreter = new Interpreter();
		$interpreter->addObserver(function(array $row) use(&$recordsChecked, &$recordsValid) {
			
			// Make sure header rows are ignored
			if($row[0] == "url") {
				return;
			}
			
			$data = [
				'url' 				=> $row[0],
				'image'				=> $row[1],
				'title'				=> $row[2],
				'comments'			=> $row[3],
				'author_name'		=> $row[4],
				'author_image'		=> $row[5],
				'author_username'	=> $row[6],
				'author_profile'	=> $row[7],
				'network'			=> $row[8],
				'visibility'		=> $row[9],
				'post_date'			=> $row[10],
			];
			
			$post = new Social\Post($data);
			$post->processed = false;
			
			
			if( $post->saveIgnoreDuplicates() ) {
				$recordsValid++;
			}
			
			$recordsChecked++;
			
		});

		$lexer->parse($file, $interpreter);
		
		return $recordsValid;

	}


	/**
	 * Import CSV using MySQL's LOAD DATA INFILE (high speed and efficent).
	 * 
	 * CSV format based on TintUp CSV Export format.
	 * 
	 * Returns number of rows inserted.
	 * 
	 * @param string $file
	 * @return int
	 */
	public static function massImport($file) {
		
		$query = sprintf(
			"LOAD DATA local INFILE '%s' INTO TABLE social_posts FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES " .
			"(`url`, `image`, `title`, `comments`, `author_name`, `author_image`, `author_username`, `author_profile`, `network`, `visibility`, `post_date`)", addslashes($file)
		);

		return \DB::connection()->getpdo()->exec($query);
		
	}
	
	
}
