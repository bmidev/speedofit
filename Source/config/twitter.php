<?php
//    'search_query' 			=> App\Challenge::isPromoActive() ? '#AxonSweepstakes' : '#TestDev',
return [
    //production
    'consumer_key'    		=> '',
    'consumer_secret' 		=>  '',

    //local
    'consumer_key_local'    		=> '',
    'consumer_secret_local' 		=>  '',

    //staging
    'consumer_key_staging'    		=> '',
    'consumer_secret_staging' 		=>  '',

    'oauth_token'        	=>  null,
    'oauth_token_secret'	=>  null,

    'bearer_token'        	=> null,
    
    'callback_url'        	=>  'twitter/callback',
    'fallback_url'      	=>  '/',
    
	
    'search_query' 			=> '',


    'start_date' => '2015-07-27 00:00:00',
    'end_date' => '2015-08-31 23:59:59',

    //share
    'share_copy' => 'I just entered #festool12tools of Christmas Competition. Enter now for your chance to win',
    'share_url' => 'http://festool12tools.co.uk'
];
