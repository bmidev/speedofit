<?php
ini_set('date.timezone', 'Asia/Kolkata');

return [
	'dash_app_name' => 'Brandmovers',
	'dash_country_filter' => 'India',
	'dash_country_chart' => 'IN',
	/*
	  |
	  |--------------------------------------------------------------------------
	  | Google Analytics Information
	  |--------------------------------------------------------------------------
	  |
	  |	Call each of these variables using gaEmail(), gaPassword(), or
	  |	gaProfileId() respectively.
	  |
	  |--------------------------------------------------------------------------
	  | Where to find the Google Analytics profile ID
	  |--------------------------------------------------------------------------
	  |
	  |   To get the ga_profile_id config item, log into http://google.com/analytics
	  |   with the Brandmover's e-mail and password (below), and then click
	  |   into the site for your project. The URL will look something like:
	  |
	  |   https://www.google.com/analytics/web/?hl=en#report/visitors-overview/a30383075w56852320p57967808/
	  |
	  |   The ga_profile_id is the last set of numbers after the 'p' in the url
	  |   (in this example, 57967808).
	  |
	 */
	'ga_email' => 'brandmoversindia@bmindiaanalytics.iam.gserviceaccount.com',
	'ga_password' => "MIIJqAIBAzCCCWIGCSqGSIb3DQEHAaCCCVMEgglPMIIJSzCCBXAGCSqGSIb3DQEHAaCCBWEEggVdMIIFWTCCBVUGCyqGSIb3DQEMCgECoIIE+jCCBPYwKAYKKoZIhvcNAQwBAzAaBBT4UyR6xAMAvHfo67E3HEq6HCF7NQICBAAEggTIEMq3zb3k5ZkHlCSiDqh3gGTDR/gPoJR6q2TcUrXAJMoNzt5KIIl6l7l/BZQ22ugAg/WST4+7VlXULz3V4PoCqLmWOUvUw/N6dBtY+Tuu6+MSiPAplwmHoU69ToqYOHnULneDJIaPPMyQKathpd0s+vjWXXzw6Jwz6OFmpdyqkbH7ZJlTzQnal4eF6FiZlCXvQb+9RABjfrT0RPqFHD7dwv1Ujfk7FofxgjRcqQwb3PYqe7+beufkb6s5GRzTvPOhhdGruEB4AW8nsPcP8WsOKAPr4HXUXlrL2ho4wvLRDaAXEWGxkiGxMb8Y1MtSfd+lbB0iCxi0VnaHGwiNfJaGa7G1GPftWhiYxyn8rsLgB5rbw9WW85wczvTMPvGGYSx5fjHYRLgw2Gd8oXqO/aCmPd6prGEM8VwftSJ5I0ZR534gqMWjPhQ63w6qYn1J9Oxpsh0F59TKpNPQSJjqykpm7sQDoHiSx1L2ADjVSuK6G2153EPL0FLQ/mnP47WQzs+eBur8JDzZKj4Sd0pMPxdDCsJlmSzAtaoHz2hkCBjQ47ImLVQB679SR0qAoKYFbRUm5AeAoSsqoEwDD6pybhZqKgIBOEo7mhaXmTO9EDg27H/OFpTUgCt7n3You0eIRZ3xpV79xCeE/r3Nq90UDX16F2deTOudME+G+Tnk9Fxd6s85XcqRKXmbsocvqRKxRu05CnxMe7rUchkTX6BXoNSwu0zzyGaAezhZ0Kr2EhIRIpUxNhTAQ7LdhCQRi73V15O7jcIlgSMZ84EVOFzCUtPrEObxqHlNt70SAUFJnAag5NDgkUzmDPzn06gIxbFK2qJwDKrOc4jbGDOQMj61L04jC9cZorgCla4oNcoN61GmTyTVUj8win55VaWXry6uEBfKn+2KnHR33qMHzhfG0Cw70G1h1woo0ZqzlXnLBeCoV+kPrhrM4kVBrqQu582ciKX3lcO7yC1UdGU9h2j0x+j5sOUB6njlaM+mr/f6qRGC41WrMDmwMbAZmcqbYGADfS/wFl3Or0eifJm9qpRsNxUxFAccvuHjPPqWszk13xoXiFqELGeB/ql5TfWlqkfnCISMdOZkQEp87DK4Nn1rdccC3+TC4glCz02tALR5lWaz4t1ZhIQfwrlEJCnhJ5qVxsZOTSOwQHg3xuHVFWhAL1xGSVfPhrpQPjSTA9FwlS7Mccyn16EqkG5+Y6s5rwg8zCN8KAYQq165Xugg7JDeKp2K8+YsCHLsBhUKyijBNsK+H7WYkmLSYLyL+I6m5LiqdAL8M74znnscXSk4CHqCBKTjlozOeND/z2iWyM2moXMIWgPT5PeGVReTKWnSMqJyZr2wyvY2JeMGuz7asQAQm6qhDFVaPEX0rJ+QWEvyt8Onk73t/X2nFvzXyia7ev4XkhZFFWM/9hXC2VIMQo3kabnefrJUFZQfbvv+diEoO7TKFw2V9LuP8f8QKYQkivodqfp95oQ8eY0bJOar5RoUSV085oeYieI9B3ZEYktB++pikt1epraGHQWVOAWeh3ONEz9MI+3dNVJHv9rZWzGQFnImD4t1Jk5rwVKgRtf7eyn9WG8yi0xJYhJq879s7nJUrsq6NZcyIcefYSMWF5rD5iiniFrtXTztSbVtMUgwIwYJKoZIhvcNAQkUMRYeFABwAHIAaQB2AGEAdABlAGsAZQB5MCEGCSqGSIb3DQEJFTEUBBJUaW1lIDE0NTg2NDM2ODQwNzQwggPTBgkqhkiG9w0BBwagggPEMIIDwAIBADCCA7kGCSqGSIb3DQEHATAoBgoqhkiG9w0BDAEGMBoEFDaLtPfEGCSpicLhwzPxB1v1FWKiAgIEAICCA4AB40VLFhf/eR9zJqq4tOrFOZ0UtbQyWUstEeNktmOyzpPPY8J+V3wDfDJdR3O2awOhOIp0ZB6NnVhajxJk6QMobtCROEbAScw6S6I/hKZ4TsFhvm6baTQwXOQelLy846Dm3OxwotFr354HHwgmK9Y72arm8uunCkFOr4SM29eh8BKXgLyThZLAYFrBMvL9fxl2F/Fsb6j9sx7qnogobANMSslpw1MkaqTSrLMdWT4p1QIRx1hn9eTcR11OtMkNiIiO1zQcYQOMLNZbRyRs2WA/aDhMf69yI88LB2lJ8zEiixBgNNdTGAwYzJp+WmSCYdj0Nv1mi4q8ptVUmTf7SwboJHTkMzjxY5mWNYtYCmo2WarG0IHEhdZ+D4b9HubqFccztG4ymPWnII9FE3tsoohwKzbgpjwcrx86EkBdCCsTaB4axdijtJ6+pdVhT95V2Yx7tcDfS4LaGRuzYyDH5tzSxNXzoTg2quQds9aIZpQ0ijEGeD5CelwKeu4sfOfXAM90Pn6LSbq7SCcV/ijhvzrOQ5CT2xuXKZVbydGzdDxElLPtbVklnxpGSLyG2gTPLhUSOZ3Tr4ZVvw2q89/yRyCPF2kDs0Y7cJb6z5W0qCKyAlhyDMZR3JZma7eGf1HPndMK7gyALO/eNzA/IH4eNkhG3VMgdzFvIO85ePAQ/3SnGWYIGgTcGWIhiVLopxMGQiFj+tUYTpd2a7Cmd6Rj04pR3dVrqvblPK8DtWdHUFyN1AqX34eJXchp7lSvXd//C3VMYtWPek7t6UZuKYiUVzrUrzPYgusABuZ5bQYvfcigjr6gueisfVdoFQlukj/8RBBXZZMge8fEQNw9WniV+DiCet0qPW7kek8hEiEzDHZOEVXoTm4T27t3AHuWbh2450h3sBXBva9nB0Zj99y4p4dgMatqqzvpSKXwS/CsLegxbzK+6j/BOiIvswTI9VaX38BdZ1OqCm92/55hhXU1xinzn+338bG3+PCdYFO967hL7cgtwqPL/b7jr7JoSrM+uvjMf3pL8Ns/LczcJFul/SQhGet3ee1DmrYEKyqKdQWd+sUF9WdVK9neA/fI6EmDodxgfHPvdwGXxdf0JCaL7cfE28lptd/HE0M+0szjWU2bBjgn4Q8mc+Uw3S8NObTSzvrlDoJyRr7hOMUFal4xaAAiezL3m5b8ZPbxqquFU/ELCTA9MCEwCQYFKw4DAhoFAAQUs8e9CMlzIA4RIDR9meiZJ4lF1BEEFHL4vp2g8twVsyNBCYhrPqHlPsIgAgIEAA==",
	'ga_profile_id' => '118932257', // Mandatory

	/*
	 * Admin title
	 * Displays in page title and header
	 */
	'title' => 'Brandmovers administrator',
	/*
	 * Admin url prefix
	 */
	'prefix' => 'admin',
	/*
	 * Before filters to protect admin from unauthorized users
	 */
	'beforeFilters' => ['admin.auth'],
	/*
	 * Path to admin bootstrap files directory in app directory
	 * Default: 'app/admin'
	 */
	'bootstrapDirectory' => app_path('admin'),
	/*
	 * Path to images directory
	 * Default: 'public/images'
	 */
	'imagesDirectory' => public_path('images'),
	/*
	 * Path to files directory
	 * Default: 'public/files'
	 */
	'filesDirectory' => public_path('files'),
	/*
	 * Path to images upload directory within 'imagesDirectory'
	 * Default: 'uploads'
	 */
	'imagesUploadDirectory' => 'uploads',
	/*
	 * Authentication config
	 */
	'auth' => [
		'model' => '\Brandmovers\AdminAuth\Entities\Administrator',
		'rules' => [
			'username' => 'required',
			'password' => 'required',
		]
	],
	/*
	 * Blade template prefix, default admin::
	 */
	'bladePrefix' => 'admin::',
	/*
	 * Caching on or off, default TRUE
	 */
	'caching' => TRUE,

	'app_start_date' => strtotime('2016-04-14 00:00:00'),
        
	'app_end_date' => strtotime('2016-05-30 00:00:00')
];
