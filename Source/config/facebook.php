<?php

return [
    //production
    'fb_app_id'     => '980496892005910',
    'fb_app_secret' => 'a7a20934a79275de83fb938f9950b635',

    //staging
    'fb_app_id_staging'  => '980497612005838',
    'fb_app_secret_staging'  => 'b13e703c5fb563c28a679ea3d9ba52a8',

    //local
    'fb_app_id_local'  => '980497962005803',
    'fb_app_secret_local'  => '51f007135232d340f5ebad2f6c97ee15',

    'share_copy' => "I've just entered the Festool 12 Tools of Christmas Competition for a chance to win fantastic Festool tools on a daily basis from 11th - 22nd December 2015. Enter now for your chance to win this Christmas too!"

];