<?php

use Illuminate\Database\Migrations\Migration;

class AddMetricsToMetricsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        // [START] Swimmer Metric Data
        DB::table('metrics')->insert([
            'metric' => 'h',
            'value' => 95,
            'is_swimmer'   => 1,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        
        DB::table('metrics')->insert([
            'metric' => 'm',
            'value' => 80,
            'is_swimmer'   => 1,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        
        DB::table('metrics')->insert([
            'metric' => 'l',
            'value' => 60,
            'is_swimmer'   => 1,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table('metrics')->insert([
            'metric' => 's',
            'value' => 50,
            'is_swimmer'   => 1,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        // [END] Swimmer Metric Data
        
        // [START] Non-Swimmer Metric Data
        DB::table('metrics')->insert([
            'metric' => 'h',
            'value' => 75,
            'is_swimmer'   => 0,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        
        DB::table('metrics')->insert([
            'metric' => 'm',
            'value' => 65,
            'is_swimmer'   => 0,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        
        DB::table('metrics')->insert([
            'metric' => 'l',
            'value' => 50,
            'is_swimmer'   => 0,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table('metrics')->insert([
            'metric' => 's',
            'value' => 40,
            'is_swimmer'   => 0,
            'is_active' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        // [END] Non-Swimmer Metric Data
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        DB::table('metrics')->truncate();
    }
}
