<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnswerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('answers', function(Blueprint $table){
            $table->increments('id');
            $table->integer('question_id')->unsigned();
            $table->longText('answer');
            $table->enum('metric', ['h', 'm', 'l', 's']);
            $table->timestamps();
            $table->softDeletes();
            $table->index('question_id');
            $table->index('metric');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('answers');
    }
}
