<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('users', function(Blueprint $table){
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->string('name', 64)->nullable();
            $table->string('email', 128)->unique();
            $table->tinyInteger('is_winner')->default(0);
            $table->timestamps();
            $table->index('created_at');
            $table->index('is_winner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('users');
    }
}
