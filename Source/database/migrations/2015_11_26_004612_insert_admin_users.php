<?php

use Illuminate\Database\Migrations\Migration;

class InsertAdminUsers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        DB::table('admins')->insert([
            'role' => 'admin',
            'email'   	=> 'admin@speedofit.com',
            'password'   => Hash::make('$peedo16'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        DB::table('admins')->insert([
            'role' => 'client',
            'email'   	=> 'client@speedofit.com',
            'password'   => Hash::make('$peedo16'),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        DB::table('admins')->truncate();
    }
}
