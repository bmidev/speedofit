<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMetricTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('metrics', function(Blueprint $table){
            $table->increments('id');
            $table->enum('metric', ['h', 'm', 'l', 's']);
            $table->integer('value');
            $table->tinyInteger('is_swimmer');
            $table->tinyInteger('is_active');
            $table->timestamps();
            $table->softDeletes();
            $table->index('is_swimmer');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('metrics');
    }

}
