<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('entries', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->mediumText('name');
            $table->integer('age');
            $table->string('email');
            $table->string('gender');
            $table->string('score')->default(0);
            $table->tinyInteger('is_swimmer');
            $table->timestamps();
            $table->index('created_at');
            $table->index('email');
            $table->index('is_swimmer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('entries');
    }
}
