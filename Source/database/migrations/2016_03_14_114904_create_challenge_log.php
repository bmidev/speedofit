<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengeLog extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('challenge_logs', function(Blueprint $table){
            $table->increments('id');
            $table->integer('entry_id');
            $table->integer('question_id');
            $table->integer('answer_id');
            $table->string('metric');
            $table->timestamps();
            $table->index('entry_id');
            $table->index('question_id');
            $table->index('answer_id');
            $table->index('created_at');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('challenge_logs');
    }
}
